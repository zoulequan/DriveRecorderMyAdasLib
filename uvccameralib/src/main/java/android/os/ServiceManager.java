/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.os;

import java.util.Map;

/** @hide */
public final class ServiceManager {
    private static final String TAG = "ServiceManager";

    public static IBinder getService(String p1) {
        throw new RuntimeException("Stub!");
    }

    public static void addService(String p1, IBinder p2) {
        throw new RuntimeException("Stub!");
    }

    public static IBinder checkService(String p1) {
        throw new RuntimeException("Stub!");
    }

    public static String[] listServices() {
        throw new RuntimeException("Stub!");
    }

    public static void initServiceCache(Map p1) {
        throw new RuntimeException("Stub!");
    }
}

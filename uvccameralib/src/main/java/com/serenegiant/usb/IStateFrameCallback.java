package com.serenegiant.usb;

/**
 * 回调摄像头状态
 */
public interface IStateFrameCallback {
	public void onStates(byte[] frame);
}

package com.serenegiant.usb;

import java.nio.ByteBuffer;

public interface IStatusCallback {
    void onStates(int statusClass, int event, int selector, int statusAttribute, ByteBuffer data);
}

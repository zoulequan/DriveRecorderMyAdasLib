package com.huiying.cameramjpeg;

/**
 * Created by zoulequan on 2018/6/4.
 */

public interface LogListener {
    void writeFile(String msg);
}

package com.huiying.cameramjpeg;

import android.text.TextUtils;
import android.view.Surface;

/**
 * Created by admin on 2017/7/7.
 */

public class VideoPlayer {

    static{
       // System.loadLibrary("jpeg-turbo1500");
       // System.loadLibrary("videoplay-lib");
    }

    private long mNativePtr=0;
    public VideoPlayer(){
        mNativePtr=nativeCreate();
    }
    public void setVideoPath(String path){
        if(mNativePtr!=0&&!TextUtils.isEmpty(path)){
            nativeSetVideoPath(mNativePtr,path);
        }
    }
    public void setDisplaySurface(Surface surface){
        if(mNativePtr!=0){
            nativeSetDisplaySurface(mNativePtr,surface);
        }
    }
    public int prepair(){
        if(mNativePtr!=0){
            return nativePrepare(mNativePtr);
        }
        return -1;
    }
    public void start(){
        if(mNativePtr!=0){
            nativeStart(mNativePtr);
        }
    }
    public void stop(){
        if(mNativePtr!=0){
            nativeStop(mNativePtr);
        }
    }

    public void close(){
        if(mNativePtr!=0){
            nativeClose(mNativePtr);
        }
    }
    public void release(){
        if(mNativePtr!=0){
            nativeRelease(mNativePtr);
            mNativePtr=0;
        }
    }

    public int getDuration(){
        if(mNativePtr!=0){
            return nativeGetDuration(mNativePtr);
        }
        return 0;
    }

    public int getCurrentPos(){
        if(mNativePtr!=0){
            return nativeGetCurrentPos(mNativePtr);
        }
        return 0;
    }

    public void setSpeed(int speed){
        if(mNativePtr!=0){
            nativeSetSpeed(mNativePtr,speed);
        }
    }

    public void setStateCallback(StatePlayCallback callback){
        if(mNativePtr!=0){
            nativeSetStateCallback(mNativePtr,callback);
        }
    }

    public void initGles(int width, int height) {
        nativeInitGles(mNativePtr,width,height);
    }

    public void changeESLayout(int width, int height) {
        nativeChangeESLayout(mNativePtr,width,height);
    }

    public void drawESFrame() {
        nativeDrawESFrame(mNativePtr);
    }
    private native long nativeCreate();
    private native void nativeSetVideoPath(long id_player,String videoPath);

    private native void nativeSetDisplaySurface(long id_player, Surface surface);
    private native int nativePrepare(long id_player);
    private native void nativeStart(long id_player);
    private native void nativeStop(long id_player);
    private native void nativeClose(long id_player);
    private native void nativeRelease(long id_player);
    private native void nativeSetStateCallback(long id_player,StatePlayCallback callback);
    private native int nativeGetDuration(long id_player);
    private native int nativeGetCurrentPos(long id_player);

    private native void nativeInitGles(long id_player,int width, int height);

    private native void nativeChangeESLayout(long id_player,int width, int height);

    private native void nativeDrawESFrame(long id_player);

    private native void nativeSetSpeed(long id_player,int speed);
}

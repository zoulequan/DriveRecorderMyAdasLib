package com.huiying.cameramjpeg;

/**
 * Created by admin on 2017/8/29.
 */

public interface ProgressCallback {
    void onProgress(int progress);
}

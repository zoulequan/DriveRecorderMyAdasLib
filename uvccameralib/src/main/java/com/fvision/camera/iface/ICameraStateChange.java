package com.fvision.camera.iface;

/**
 * Created by mayn on 2017/12/14.
 */

public interface ICameraStateChange {
    void stateChange();
}

package com.fvision.camera.iface;

/**
 * Created by mayn on 2017/12/14.
 */

public interface ISmallVideoBack {
    void success(String videoPath);
    void downloadProgress(float progress);
    void fail(int code, String errorMsg);
}

package com.fvision.camera.iface;


import com.fvision.camera.bean.FileBean;

import java.util.List;

/**
 * Created by zoulequan on 2018/7/16.
 */

public interface IGetPlackBack {
    void onFail(int code, String msg);
    void onProgress(float progress);
    void onSuccess(List<FileBean> list);
}

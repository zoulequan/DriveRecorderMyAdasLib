package com.fvision.camera.iface;


/**
 * Created by zoulequan on 2018/7/16.
 */

public interface ICoreClientCallback {
//    void onCSCreate();
//
//    void onCSDestory();

    void onConnect();

    void onDisconnect();

    void initCmd(boolean isSuccess,String msg);

    void onIsAvailable(boolean var1);

    void onInit(boolean var1, int var2, String var3);
}

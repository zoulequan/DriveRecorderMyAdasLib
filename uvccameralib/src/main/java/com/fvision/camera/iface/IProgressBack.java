package com.fvision.camera.iface;


/**
 * Created by zoulequan on 2018/7/16.
 */

public interface IProgressBack {
    void onFail(int code, String msg);
    void onProgress(float progress);
    void onSuccess(String path);
}

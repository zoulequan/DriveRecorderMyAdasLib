package com.fvision.camera.bean;

/**
 * Created by zoulequan on 2018/12/20.
 */

public class DevFileName {
    private String preView;
    private String cmd;
    private String packageName;

    public DevFileName(String pre, String cmmd, String pkg){
        preView = pre;
        cmd = cmmd;
        packageName = pkg;
    }

    public String getPreView() {
        return preView;
    }

    public String getCmd() {
        return cmd;
    }

    public void setPreView(String preView) {
        this.preView = preView;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}

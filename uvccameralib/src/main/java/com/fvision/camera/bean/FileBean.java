package com.fvision.camera.bean;

import java.io.Serializable;

/**
 * Created by admin on 2016/12/28.
 */

public class FileBean implements Serializable {
    public String fileName;
    public int fileType = -1;
    public int fileIndex;
    public boolean isLock;
    public long sorttime = 0L;
    public long dayTime = 0L;

    public int year;
    public int month;
    public int day;
    public int hour;
    public int minute;
    public int sencond;

    @Override
    public String toString() {
        return "FileBean{" +
                "fileName='" + fileName + '\'' +
                ", fileType=" + fileType +
                ", fileIndex=" + fileIndex +
                ", isLock=" + isLock +
                ", sorttime=" + sorttime +
                ", dayTime=" + dayTime +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", hour=" + hour +
                ", minute=" + minute +
                ", sencond=" + sencond +
                '}';
    }
//    public FileInfo fileinfo;
}

package com.fvision.camera.bean;

import android.util.Log;

/**
 * Created by mayn on 2017/12/13.
 */

public class CameraStateBean {

//    CAM_REC_STATE   = 0x4000,//录像
//    CAM_LOCK_STATE  = 0x2000,//加锁
//    CAM_PLAY_STATE  = 0x1000, //播放
//    CAM_WATER_STATE = 0x0800,//水印
//    CAM_MUTE_STATE  = 0x0400,//静音
//    CAM_SD_STATE    = 0x0200,//SD
//    CAM_PIC_STATE   = 0x0100,//拍照
//    CAM_MOTION_DETECT=0x0080,//移动侦测
//    CAM_FORMAT_STATE=0x0040,//格式化
//    CAM_PLIST_STATE=0x0020,	//更新文件列表
//    CAM_TF_CU_STATE=	0x0010,//cluster  0: need format , 1: is ok

    /**
     * 回放
     */
    boolean cam_mode_state = false;
    /**
     * 录像
     */
    boolean cam_rec_state = false;
    /**
     * 加锁
     */
    boolean cam_lock_state = false;
    /**
     * 播放
     */
    boolean cam_play_state = false;
    /**
     * 水印
     */
    boolean cam_water_state = false;
    /**
     * 静音
     */
    boolean cam_mute_state = false;
    /**
     * SD
     */
    boolean cam_sd_state = false;
    /**
     * 拍照
     */
    boolean cam_pic_state = false;
    /**
     * 移动侦测
     */
    boolean cam_motion_detect = false;
    /**
     * 格式化
     */
    boolean cam_format_state = false;
    /**
     * 更新文件列表
     */
    boolean cam_plist_state = false;

    /**
     * 是否需要格式化
     */
    boolean cam_tf_cu_state = false;

    /**
     * 回放文件的总时长 unit second
     */
    int total_time = -1;
    /**
     * 回放文件时当前已播放的时长 unit second
     */
    int cur_time = -1;
    /**
     * duration(rec time ) | gsensor_level (4bit),执行循环录像时长和重力感应的灵敏度的数据保存在这里，以便apk获取数据结果
     */
    int duration_gsensor = -1;
    /**
     * no use
     */
    int save_1;
    /**
     * 固件版本
     */
    byte[] deviceVersion = null;
    /**
     * 固件版本包名
     */
    byte[] devicePackageName = null;

    /**
     * 固件版本Code
     */
    byte[] deviceVersionCode = null;
    /**
     * PLIST 里的文件数量
     */
    int file_index = -1;
    /**
     * 播放速度 取值范围 0-8 对应 速度倍数
     */
    int playSpeed = -1;

    /**
     * adas状态
     */
    int adasState = -1;

    /**
     * 原始数据
     */
    byte[] stateFrame = null;

    public boolean isCam_mode_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_mode_state = (num & 0x8000) > 1;
        return cam_mode_state;
    }

    public boolean isCam_rec_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_rec_state = (num & 0x4000) > 1;
        return cam_rec_state;
    }

    public boolean isCam_lock_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_lock_state = (num & 0x2000) > 1;
        return cam_lock_state;
    }

    public boolean isCam_play_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_play_state = (num & 0x1000) > 1;
        return cam_play_state;
    }

    public boolean isCam_water_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_water_state = (num & 0x0800) > 1;
        return cam_water_state;
    }

    public boolean isCam_mute_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_mute_state = (num & 0x0400) > 1;
        return cam_mute_state;
    }

    public boolean isCam_sd_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_sd_state = (num & 0x0200) > 1;
        return cam_sd_state;
    }

    public boolean isCam_pic_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_pic_state = (num & 0x0100) > 1;
        return cam_pic_state;
    }

    public boolean isCam_motion_detect() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_motion_detect = (num & 0x0080) > 1;
        return cam_motion_detect;
    }

    public boolean isCam_format_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_format_state = (num & 0x0040) > 1;
        return cam_format_state;
    }

    public boolean isCam_plist_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_plist_state = (num & 0x0020) > 1;
        return cam_plist_state;
    }

    public boolean isCam_tf_cu_state() {
        if (stateFrame == null) {
            return false;
        }
        short num = byte2Short(0, 2);
        cam_tf_cu_state = (num & 0x0010) == 0;
        return cam_tf_cu_state;
    }

    public int getTotal_time() {
        if (stateFrame == null) {
            return -1;
        }
        total_time = byte2Short(2, 2);
        return total_time;
    }

    public int getCur_time() {
        if (stateFrame == null) {
            return -1;
        }
        cur_time = byte2Short(4, 2);
        return cur_time;
    }

    public int getDuration_gsensor() {
        if (stateFrame == null) {
            return -1;
        }
        duration_gsensor = stateFrame[6];
        return duration_gsensor;
    }

    public int getSave_1() {
        if (stateFrame == null) {
            return -1;
        }
        save_1 = stateFrame[7];
        return save_1;
    }

    public int getFile_index() {
        if (stateFrame == null) {
            return -1;
        }
        file_index = byte2Short(8, 2);
        return file_index;
    }

    public String getPasswd() {
        if (deviceVersion == null) {
            deviceVersion = new byte[10];
        }
        if (stateFrame == null) {
            return "";
        }
        System.arraycopy(stateFrame, 28, deviceVersion, 0, deviceVersion.length);

        StringBuffer version = new StringBuffer();
        for (byte bt : deviceVersion) {
            version.append("" + (char) bt);
        }
        return version.toString();
    }

    public int getPlaySpeed() {
        if (stateFrame == null) {
            return -1;
        }
        playSpeed = byte2Short(38, 2);
        return playSpeed;
    }

    public int getAdasState() {
        if (stateFrame == null) {
            return -1;
        }
        adasState = getAdasState(stateFrame);
        return adasState;
    }

    public int getAdasState(byte[] frame) {
        if (frame == null) {
            return -1;
        }
        return frame[50];
    }

    public byte[] getStateFrame() {
        return stateFrame;
    }

    public void setStateFrame(byte[] stateFrame) {
        this.stateFrame = stateFrame;
    }

    private int byte2int(int startPos, int length) {
        byte[] newByte = new byte[length < 4 ? 4 : length];
        System.arraycopy(stateFrame, startPos, newByte, 0, length);
//        LogUtils.d(" newByte "+CameraStateUtil.bytesToHexString(newByte));
        return bytesToInt_hight2low(newByte, 0);
    }

    private short byte2Short(int startPos, int length) {
        byte[] newByte = new byte[length < 2 ? 2 : length];
        System.arraycopy(stateFrame, startPos, newByte, 0, length);
//        LogUtils.d(" newByteShort "+CameraStateUtil.bytesToHexString(newByte));
        return bytesToShort(newByte, 0);
    }

    private short byte2Short(byte[] frame, int startPos, int length) {
        byte[] newByte = new byte[length < 2 ? 2 : length];
        System.arraycopy(frame, startPos, newByte, 0, length);
//        LogUtils.d(" newByteShort "+CameraStateUtil.bytesToHexString(newByte));
        return bytesToShort(newByte, 0);
    }

    public void print() {
        Log.d("zoulequan", toString());
    }

    public String toString() {
        String str = " 回放:" + isCam_mode_state() +
                " 录像:" + isCam_rec_state() +
                " 加锁:" + isCam_lock_state() +
                " 播放:" + isCam_play_state() +
                " 水印:" + isCam_water_state() +
                " 静音:" + isCam_mute_state() +
                " SD:" + isCam_sd_state() +
                " 拍照:" + isCam_pic_state() +
                " 移动侦测:" + isCam_motion_detect() +
                " 格式化:" + isCam_format_state() +
                " 是否需要格式化:" + isCam_tf_cu_state() +
                " 更新文件列表:" + isCam_plist_state() +
                " 回放文件的总时长:" + getTotal_time() +
                " 回放文件时当前已播放的时长:" + getCur_time() +
                " duration_gsensor:" + getDuration_gsensor() +
                " save_1:" + getSave_1() +
                " 版本号:" + getPasswd() +
                " PLIST 里的文件数量:" + getFile_index() +
                " 播放速度:" + getPlaySpeed() +
                " ADAS状态:" + getAdasState();
        return str;
    }

    public boolean isMainStateChange(byte[] frame) {
        if (isCam_mode_state(frame) != isCam_mode_state()) {
            return true;
        }
        if (isCam_rec_state(frame) != isCam_rec_state()) {
            return true;
        }
        if (isCam_lock_state(frame) != isCam_lock_state()) {
            return true;
        }
        if (isCam_play_state(frame) != isCam_play_state()) {
            return true;
        }
        if (isCam_water_state(frame) != isCam_water_state()) {
            return true;
        }
        if (isCam_mute_state(frame) != isCam_mute_state()) {
            return true;
        }
        if (isCam_sd_state(frame) != isCam_sd_state()) {
            return true;
        }
        if (isCam_pic_state(frame) != isCam_pic_state()) {
            return true;
        }
        if (isCam_motion_detect(frame) != isCam_motion_detect()) {
            return true;
        }
        if (isCam_format_state(frame) != isCam_format_state()) {
            return true;
        }
        if (getCur_time(frame) != getCur_time()) {
            return true;
        }
        if (getSave_1(frame) != getSave_1()) {
            return true;
        }
        if (getAdasState(frame) != getAdasState()) {
            return true;
        }
        return false;
    }

    public int getSave_1(byte[] frame) {
        return frame[7];
    }

    public boolean isCam_mode_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_mode_state = (num & 0x8000) > 1;
        return cam_mode_state;
    }

    public boolean isCam_rec_state(byte[] frame) {
//        Log.e("goPlayBackMode", "" + frame);
        short num = byte2Short(frame, 0, 2);
        cam_rec_state = (num & 0x4000) > 1;
        return cam_rec_state;
    }

    public boolean isCam_lock_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_lock_state = (num & 0x2000) > 1;
        return cam_lock_state;
    }

    public boolean isCam_play_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_play_state = (num & 0x1000) > 1;
        return cam_play_state;
    }

    public boolean isCam_water_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_water_state = (num & 0x0800) > 1;
        return cam_water_state;
    }

    public boolean isCam_mute_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_mute_state = (num & 0x0400) > 1;
        return cam_mute_state;
    }

    public boolean isCam_sd_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_sd_state = (num & 0x0200) > 1;
        return cam_sd_state;
    }

    public boolean isCam_pic_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_pic_state = (num & 0x0100) > 1;
        return cam_pic_state;
    }

    public boolean isCam_motion_detect(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_motion_detect = (num & 0x0080) > 1;
        return cam_motion_detect;
    }

    public boolean isCam_format_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_format_state = (num & 0x0040) > 1;
        return cam_format_state;
    }

    public boolean isCam_plist_state(byte[] frame) {
        short num = byte2Short(frame, 0, 2);
        cam_plist_state = (num & 0x0020) > 1;
        return cam_plist_state;
    }

    public int getCur_time(byte[] frame) {
        cur_time = byte2Short(frame, 4, 2);
        return cur_time;
    }

    private int bytesToInt_hight2low(byte[] src, int offset) {
        int value;
        value = (int) ((src[offset] & 0xFF)
                | ((src[offset + 1] & 0xFF) << 8)
                | ((src[offset + 2] & 0xFF) << 16)
                | ((src[offset + 3] & 0xFF) << 24));
        return value;
    }

    private short bytesToShort(byte[] src, int offset) {
        short value;
        value = (short) ((src[offset] & 0xFF)
                | ((src[offset + 1] & 0xFF) << 8)
        );
        return value;
    }

    //获取固件版本
    public String getDevVersion() {
        if (deviceVersion == null) {
            deviceVersion = new byte[12];
        }
        if (stateFrame == null) {
            return "";
        }
        System.arraycopy(stateFrame, 28, deviceVersion, 0, deviceVersion.length);

        StringBuffer version = new StringBuffer();
        for (byte bt : deviceVersion) {
            version.append("" + (char) bt);
        }
        return version.toString();
    }


    //获取固件包名
    public String getDevPackageName() {
        if (devicePackageName == null) {
            devicePackageName = new byte[64];
        }
        if (stateFrame == null) {
            return "";
        }
        System.arraycopy(stateFrame, 60, devicePackageName, 0, devicePackageName.length);
//        StringBuffer versionPackageName = new StringBuffer();
//        for (byte bt:devicePackageName){
//            versionPackageName.append(""+(char)bt);
//        }
        return new String(devicePackageName).trim();
    }

    //获取固件版本号
    public int getDevVersionCode() {
        if (deviceVersionCode == null) {
            deviceVersionCode = new byte[4];
        }
        if (stateFrame == null) {
            return -1;
        }
        System.arraycopy(stateFrame, 56, deviceVersionCode, 0, deviceVersionCode.length);
        return bytesToInt_hight2low(deviceVersionCode, 0);
    }

}

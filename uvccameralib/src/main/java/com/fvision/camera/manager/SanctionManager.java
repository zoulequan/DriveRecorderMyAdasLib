package com.fvision.camera.manager;

/**
 * Created by zoulequan on 2018/12/24.
 */

public class SanctionManager {
    public static String getSanctionName(int sanction){
        String sanctionName = null;
        switch (sanction){
            case 233333:
                sanctionName = "歌诗特";
                break;
            case 233408:
                sanctionName = "沃广视";
                break;
            case 233519:
                sanctionName = "环翔";
                break;
            case 233620:
                sanctionName = "云智驾";
                break;
            case 233731:
                sanctionName = "安智享";
                break;
            case 233842:
                sanctionName = "创鑫博业";
                break;
            case 233953:
                sanctionName = "亿能";
                break;
            case 616:
                sanctionName = "不支持厂商号";
                break;
            default:
                sanctionName = "其它";
        }
        return sanctionName;
    }
}

package com.fvision.camera.manager;

import android.text.TextUtils;
import android.util.Log;

import com.fvision.camera.bean.CameraStateBean;
import com.fvision.camera.bean.DataPaket;
import com.fvision.camera.util.Cmd_Const;
import com.fvision.camera.util.LogUtils;
import com.huiying.cameramjpeg.UvcCamera;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by mayn on 2017/12/14.
 */

public class CmdManager {
    private static CmdManager _instance;
    private CameraStateBean cameraState;
    private boolean isSyncTimeSuccess = false;
    private long lastSyncTime = 0;
    private int cmdState = CMD_STATE_NOR;//记录仪状态 0：表示正常 1：表示正在抽帧，不能发送指令

    public static final int CMD_STATE_NOR = 0;
    public static final int CMD_STATE_EXTRACT = -2;

    public static String SPACE = "0";

    public static CmdManager getInstance() {
        if (_instance == null) {
            _instance = new CmdManager();
        }
        return _instance;
    }

    public int getCmdState() {
        return cmdState;
    }

    public void setCmdState(int cmdState) {
        this.cmdState = cmdState;
    }

    public CameraStateBean getCurrentState() {
        if (cameraState == null) {
            cameraState = new CameraStateBean();
        }
        return cameraState;
    }

    public void setCurrentState(CameraStateBean State) {
        cameraState = State;
    }

    public boolean getIsSyncTimeSuccess() {
        return isSyncTimeSuccess;
    }

    /**
     * 录制声音开关
     *
     * @return
     */
    public boolean recSoundToggle() {
        if (cameraState == null) {
            return false;
        }
        boolean state = cameraState.isCam_mute_state();
        byte[] buffer;
        int value = state ? Cmd_Const.MUTE_STATE_OFF : Cmd_Const.MUTE_STATE_ON;
        if (value == 0) {
            buffer = new byte[]{(byte) 0x00};
        } else {
            buffer = new byte[]{(byte) 0x01};
        }
        int ret = sendCommand(Cmd_Const.CAM_SET_MUTE, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 录像开关
     *
     * @return
     */
    public boolean recToggle() {
        if (cameraState == null) {
            return false;
        }
        boolean state = cameraState.isCam_rec_state();
        byte[] buffer;
        int value = state ? Cmd_Const.RECORD_STATE_OFF : Cmd_Const.RECORD_STATE_ON;
        if (value == Cmd_Const.RECORD_STATE_OFF) {
            buffer = new byte[]{(byte) 0x00};
        } else {
            buffer = new byte[]{(byte) 0x01};
        }
        int ret = sendCommand(Cmd_Const.CAM_SET_REC, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 录像锁开关
     *
     * @return
     */
    public boolean lockToggle() {
        if (cameraState == null) {
            return false;
        }
        boolean state = cameraState.isCam_lock_state();
        byte[] buffer;
        int value = state ? Cmd_Const.LOCK_STATE_OFF : Cmd_Const.LOCK_STATE_ON;
        if (value == 0) {
            buffer = new byte[]{(byte) 0x00};
        } else {
            buffer = new byte[]{(byte) 0x01};
        }
        int ret = sendCommand(Cmd_Const.CAM_SET_LOCK, buffer.length, buffer);
        return ret >= 0;
    }

    public boolean lockBackPlay(String fileName) {
        int ret = sendCommand(Cmd_Const.CAM_SET_BACK_PLAY_LOCK, fileName.getBytes().length, fileName.getBytes());
        return ret >= 0;
    }

    /**
     * 回放模式开关
     *
     * @return
     */
    public boolean modelToggle() {
        if (cameraState == null) {
            return false;
        }
        boolean state = cameraState.isCam_mode_state();
        int value = state ? Cmd_Const.MODEL_STATE_OFF : Cmd_Const.MODEL_STATE_ON;

        return modelToggle(value);
    }

    /**
     * 回放模式开关
     *
     * @param model Cmd_Const.MODEL_STATE_OFF Cmd_Const.MODEL_STATE_ON
     * @return
     */
    public boolean modelToggle(int model) {
        byte[] buffer = new byte[]{(byte) model};
        int ret = sendCommand(Cmd_Const.CAM_SET_MODE, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 获取文件列表
     *
     * @param data
     * @return
     */
    public int getFiles(byte[] data) {
        if (UvcCamera.getInstance().isInit()) {
            return UvcCamera.getInstance().getFile(data);
        }
        return -1;
    }

    /**
     * 设置回放文件index
     *
     * @param fileIndex
     * @return
     */
    public boolean setPlayBackFile(int fileIndex) {
        byte[] buffer = new byte[]{(byte) fileIndex};
        int ret = sendCommand(Cmd_Const.CAM_PLAY_FILE, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 设置回放文件名
     *
     * @param fileName
     * @return
     */
    public boolean setPlayBackFile(String fileName) {
        byte[] buffer = fileName.getBytes();
        int ret = sendCommand(Cmd_Const.CAM_PLAY_FILE, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 开始回放
     *
     * @return
     */
    public boolean playPlayBackFile() {
        byte[] buffer = new byte[]{0x01};
        int ret = sendCommand(Cmd_Const.CAM_SET_PLAY, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 47 获取小机器镜头连接状态
     *
     * @return
     */
    public boolean getCamIsConnectState() {
        byte[] buffer = new byte[1];
        int ret = sendCommand(47, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 暂停回放
     *
     * @return
     */
    public boolean pausePlayBackFile() {
        byte[] buffer = new byte[]{0x00};
        int ret = sendCommand(Cmd_Const.CAM_SET_PLAY, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 拍照
     *
     * @return
     */
    public boolean takePictures() {
        byte[] buffer = new byte[]{0x01};
        int ret = sendCommand(Cmd_Const.CAM_SET_PICTURE, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 同步时间
     *
     * @return
     */
    public boolean syncTime() {
        long now = System.currentTimeMillis();
        if (now - lastSyncTime < 1000 * 10) {
            return false;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int sencond = calendar.get(Calendar.SECOND);
        byte[] data = new byte[7];
        data[0] = (byte) (year & 0xff);
        data[1] = (byte) (year >> 8 & 0xff);
        data[2] = (byte) month;
        data[3] = (byte) day;
        data[4] = (byte) hour;
        data[5] = (byte) minute;
        data[6] = (byte) sencond;

        if (year < 2018) {
            return false;
        }

        int ret = sendCommand(Cmd_Const.CAM_SET_TIME, data.length, data);
//        LogUtils.d("synctime","year=" + year + ",month=" + month + ",day=" + day + ",hour=" + hour + ",minute=" + minute + ",sencond=" + sencond);
        isSyncTimeSuccess = (ret >= 0);
        if (isSyncTimeSuccess) {
            lastSyncTime = now;
        }
//        LogUtils.d("synctime","同步时间 "+isSyncTimeSuccess);
        return isSyncTimeSuccess;
    }

    /**
     * 格式化tf卡
     *
     * @return
     */
    public boolean formatTf() {
        byte[] buffer = new byte[]{0x01};
        int ret = sendCommand(Cmd_Const.CAM_SET_FORMAT, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 快放
     *
     * @return
     */
    public boolean fastForward() {
        byte[] buffer = new byte[]{0x01};
        int ret = sendCommand(Cmd_Const.CAM_SET_PLAY_FF, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 快退
     *
     * @return
     */
    public boolean fastBackward() {
        byte[] buffer = new byte[]{0x01};
        int ret = sendCommand(Cmd_Const.CAM_SET_PLAY_FB, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 删除文件
     *
     * @return
     */
    public boolean deleteFile(int fileIndex) {
        byte[] buffer = new byte[]{(byte) fileIndex};
        int ret = sendCommand(Cmd_Const.CAM_SET_DEL_FILE, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 删除文件
     *
     * @return fileName
     */
    public boolean deleteFile(String fileName) {
        byte[] buffer = fileName.getBytes();
        int ret = sendCommand(Cmd_Const.CAM_SET_DEL_FILE, buffer.length, buffer);
        return ret >= 0;
    }

    public boolean isSupportDel() {
        String currentVer = getCurrentState().getPasswd();
        if (TextUtils.isEmpty(currentVer)) {
            return false;
        }
        int startPos = currentVer.indexOf("_v");
        String str = currentVer.substring(startPos + 2, startPos + 5);
        if (str.compareTo("2.5") > 0) {
            return true;
        }
        return false;
    }

    public boolean isSupportBackPlayLock() {
        String currentVer = getCurrentState().getPasswd();
        if (TextUtils.isEmpty(currentVer)) {
            return false;
        }
        int startPos = currentVer.indexOf("_v");
        String str = currentVer.substring(startPos + 2, startPos + 5);
        if (str.compareTo("2.7") >= 0) {
            return true;
        }
        return false;
    }

    public boolean isSupportSanction() {
        String currentVer = getCurrentState().getPasswd();
        if (TextUtils.isEmpty(currentVer)) {
            return false;
        }
        int startPos = currentVer.indexOf("_v");
        String str = currentVer.substring(startPos + 2, startPos + 5);
        if (str.compareTo("3.1") >= 0) {
            return true;
        }
        return false;
    }

    public boolean isSupportVideoDuration() {
        String currentVer = getCurrentState().getPasswd();
        if (TextUtils.isEmpty(currentVer)) {
            return false;
        }
        int startPos = currentVer.indexOf("_v");
        String str = currentVer.substring(startPos + 2, startPos + 5);
        if (str.compareTo("3.2") >= 0) {
            return true;
        }
        return false;
    }

    public String getDVRUid() {
        byte[] buffer = new byte[0x10];
        int ret = sendCommand(Cmd_Const.CAM_GET_DVR_UID, buffer.length, buffer);
        if (isSuccess(ret)) {
            return bytesToHexString(buffer).replace(" ", "");
        }
        return null;
    }

    public String getUUIDCode() {
        byte[] buffer = new byte[0x10];
        int ret = sendCommand(Cmd_Const.CAM_GET_ADAS_UID, buffer.length, buffer);
        String uid = null;
        if (isSuccess(ret)) {
            uid = bytesToHexString(buffer).replace(" ", "").toUpperCase().substring(0, buffer.length);
            if (uid != null && uid.startsWith("511") && uid.length() > 14) {
                uid = "@" + uid.substring(0, 14);
            }
        }
        return uid;
    }

    public boolean setUUIDCode(String code) {
        if (code != null && code.startsWith("511") && code.length() == 14) {
            code = code + SPACE + SPACE;
        }
        if (code == null || code.length() / 2 > 10) {
            return false;
        }
        byte[] bt = new byte[0x10];
        int j = 0;
        for (int i = 0; i < code.length() / 2; i++) {
            bt[i] = (byte) strToInt(code.substring(j, j + 2));
            j += 2;
        }
        int ret = sendCommand(Cmd_Const.CAM_SET_ADAS_UID, bt.length, bt);
        return isSuccess(ret);
    }

    public boolean writeKeyStore(String key) {
        int ret = sendCommand(Cmd_Const.CAM_SET_ADAS_CODE, 0x40, key.getBytes());
        return isSuccess(ret);
    }

    public String readKeyStore() {
        byte[] buffer = new byte[0x40];
        int ret = sendCommand(Cmd_Const.CAM_GET_ADAS_CODE, buffer.length, buffer);
        if (isSuccess(ret)) {
            return new String(buffer).trim();
        }
        return null;
    }

    public boolean isSupportKaiYiAdas() {
        String adasid = getUUIDCode();
        if (TextUtils.isEmpty(adasid)) {
            return false;
        }
        if (!adasid.contains("AD48")) {
            return false;
        }
        return true;
    }

    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append("" + hv);
        }
        return stringBuilder.toString();
    }

    public long getLastSyncTime() {
        return lastSyncTime;
    }

    public int getVendor() {
        byte[] buffer = new byte[0x4];
        int ret = sendCommand(Cmd_Const.CAM_GET_VENDOR, buffer.length, buffer);
        if (isSuccess(ret)) {
            return bytesToInt_hight2low(buffer, 0);
        }
        return -1;
    }

    public boolean setVendor(int vendor) {
        int ret = sendCommand(Cmd_Const.CAM_SET_VENDOR, 0x4, intToBytes_hight2low(vendor));
        return isSuccess(ret);
    }


    public int getVideoDuration() {
        byte[] buffer = new byte[0x4];
        int ret = sendCommand(Cmd_Const.CAM_GET_VIDEO_DURATION, buffer.length, buffer);
        if (isSuccess(ret)) {
            return bytesToInt_hight2low(buffer, 0);
        }
        return -1;
    }

    public List<Integer> getCardSize() {
        byte[] total = new byte[0x4];
        byte[] rest = new byte[0x4];
        byte[] buffer = new byte[0x8];
        List<Integer> list = new ArrayList<>();
        int ret = sendCommand(48, buffer.length, buffer);
        if (isSuccess(ret)) {
            System.arraycopy(buffer, 0, total, 0, 4);
            System.arraycopy(buffer, 4, rest, 0, 4);
            list.add(bytesToInt_hight2low(total, 0));
            list.add(bytesToInt_hight2low(rest, 0));
            return list;
        }
        return null;
    }

    public int getAdasType() {
        byte[] buffer = new byte[1];
        int ret = sendCommand(Cmd_Const.CAM_GET_ADAS_TYPE, buffer.length, buffer);
        if (isSuccess(ret)) {
            return buffer[0] & 0xff;
        }
        return -1;
    }

    public boolean setVideoDuration(int duration) {
        int ret = sendCommand(Cmd_Const.CAM_SET_VIDEO_DURATION, 0x4, intToBytes_hight2low(duration));
        return isSuccess(ret);
    }

    public static int bytesToInt_hight2low(byte[] src, int offset) {
        int value;
        value = (int) ((src[offset] & 0xFF)
                | ((src[offset + 1] & 0xFF) << 8)
                | ((src[offset + 2] & 0xFF) << 16)
                | ((src[offset + 3] & 0xFF) << 24));
        return value;
    }

    public static byte[] intToBytes_hight2low(int num) {
        byte[] bt = new byte[4];
        bt[0] = (byte) num;
        bt[1] = (byte) ((num >> 8) & 0xff);
        bt[2] = (byte) ((num >> 16) & 0xff);
        bt[3] = (byte) ((num >> 24) & 0xff);
        return bt;
    }

    public static byte[] intToBytes(int num) {
        byte[] bt = new byte[4];
        bt[3] = (byte) num;
        bt[2] = (byte) ((num >> 8) & 0xff);
        bt[1] = (byte) ((num >> 16) & 0xff);
        bt[0] = (byte) ((num >> 24) & 0xff);
        return bt;
    }

    public int sendCommand(int request, int length, byte[] data) {
        //Log.e("zoulequan", "cmd request " + request);
        if (!UvcCamera.getInstance().isInit()) {
            return -1;
        }

        if(cmdState == CMD_STATE_EXTRACT
                && request != 33
                && request != 35
                && request != 36
                && request != 37
                && request != 38
                && request != 39
                && request != 40
                && request != 41
                && request != 43
                && request != 44
                && request != 45
                && request != 46){
            return CMD_STATE_EXTRACT;
        }

        return UvcCamera.getInstance().sendCmd(Cmd_Const.NOT_USE, request, Cmd_Const.NOT_USE, Cmd_Const.NOT_USE, length, data);
    }

    private boolean isSuccess(int ret) {
        return ret >= 0;
    }

    /**
     * String 转 int
     *
     * @param str
     * @return
     */
    private int strToInt(String str) {
        char[] hexs = str.toCharArray();
        String a = "";
        for (char c : hexs) {
            switch (c) {
                case '0':
                    a += "0,";
                    break;
                case '1':
                    a += "1,";
                    break;
                case '2':
                    a += "2,";
                    break;
                case '3':
                    a += "3,";
                    break;
                case '4':
                    a += "4,";
                    break;
                case '5':
                    a += "5,";
                    break;
                case '6':
                    a += "6,";
                    break;
                case '7':
                    a += "7,";
                    break;
                case '8':
                    a += "8,";
                    break;
                case '9':
                    a += "9,";
                    break;
                case 'A':
                    a += "10,";
                    break;
                case 'B':
                    a += "11,";
                    break;
                case 'C':
                    a += "12,";
                    break;
                case 'D':
                    a += "13,";
                    break;
                case 'E':
                    a += "14,";
                    break;
                case 'F':
                    a += "15,";
                    break;
            }
        }
        String[] b = a.split(",");
        int c = Integer.valueOf(b[0]) * 16;
        int d = Integer.valueOf(b[1]) * 1;
        int e = c + d;
        return e;
    }

    public int checkUpgradeFile() {
        byte[] buffer = new byte[1];
        if (UvcCamera.getInstance().isInit()) {
            int ret = sendCommand(37, buffer.length, buffer);
//        Log.e("checkUpgradeFile", bytesToHexString(buffer) + ret);
            if (ret == 0) {
                return buffer[0] & 0xff;
            }

            return ret;
        }
        return -1;
    }

    public int startUpgrade() {
        byte[] buffer = new byte[0x04];
        int ret = sendCommand(38, 1, buffer);
        if (isSuccess(ret)) {
            return bytesToInt_hight2low(buffer, 0);
        }
        return ret;
    }

    /**
     * 是否支持固件升级
     *
     * @return
     */
    public boolean isSupperDevUpgrade() {
        String currentVersion = getCurrentState().getDevVersion();
        if (TextUtils.isEmpty(currentVersion)) {
            return false;
        }
        int startPos = currentVersion.indexOf("_v");
        //Log.e("isSupperDevUpgrade", "" + startPos + currentVersion.length());
        String str = currentVersion.substring(startPos + 2, startPos + 6);
        if (str.compareTo("2.3") < 0) {
            return false;
        }
        return true;
    }

    public boolean openWriteFile() {
        return openFile();
    }

    /**
     * 打开文件
     *
     * @return
     */
    private boolean openFile() {
        byte[] data = new byte[0x4];
//        byte[] filedata = filename.getBytes();
//        System.arraycopy(filedata,0,data,0,filedata.length);
        int ret = sendCommand(33, 1, data);
        return isSuccess(ret);
    }

    /**
     * 写数据
     *
     * @param offset 第几个包
     * @return
     */
    public int writeFileData(byte[] data, int offset) {
        int ret = sendCommand(35, data.length, data);
        return ret;
    }

    /**
     * 关闭文件  need playback mode
     *
     * @return
     */
    public boolean closeReadFile() {
        byte[] buffer = new byte[1];
        int ret = sendCommand(36, 1, buffer);
        return ret >= 0;
    }

    /**
     * 读取数据 need playback mode
     *
     * @param offset 第几个包
     * @return
     */
    public DataPaket getFileData(int offset, int length) {
        byte[] buffer = new byte[length];
        int ret = sendCommand(40, buffer.length, buffer);
        //LogUtils.d("downfile 读取数据返回 "+ret + "length "+buffer.length);
        if (isSuccess(ret)) {
            DataPaket paket = new DataPaket();
            paket.data = buffer;
            paket.length = buffer.length;
            return paket;
        }
        return null;
    }

    /**
     * 读取数据 need playback mode
     *
     * @param offset 第几个包
     * @return
     */
    public DataPaket getVideoFileData(int offset, int length) {
        byte[] buffer = new byte[length];
        int ret = sendCommand(45, buffer.length, buffer);
        //LogUtils.d("downfile 读取数据返回 "+ret + "length "+buffer.length);
        if (isSuccess(ret)) {
            DataPaket paket = new DataPaket();
            paket.data = buffer;
            paket.length = buffer.length;
            return paket;
        }
        return null;
    }


    /**
     * 关闭文件  need playback mode
     *
     * @return
     */
    public boolean closeDownloadFile() {
        byte[] buffer = new byte[1];
        int ret = sendCommand(41, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 关闭文件  need playback mode
     *
     * @return
     */
    public boolean closeDownloadZhenFile() {
        byte[] buffer = new byte[1];
        int ret = sendCommand(46, buffer.length, buffer);
        return ret >= 0;
    }

    /**
     * 打开要写入的文件
     *
     * @return
     */
    public boolean openWriteFiles() {
        return openFiles();
    }

    /**
     * 打开读取文件  need playback mode
     *
     * @param type     0: DCIM, 正常1: EVENT, 緊急 2: LOCK, 加鎖 3: JPEG, 拍照
     * @param filename 文件名
     * @return 文件大小
     */
    public int openReadFile(int type, String filename) {
        return openFiles(filename);
    }

    /**
     * 打开文件
     *
     * @return
     */
    private boolean openFiles() {
        byte[] data = new byte[0x4];
//        byte[] filedata = filename.getBytes();
//        System.arraycopy(filedata,0,data,0,filedata.length);
        int ret = sendCommand(33, 1, data);
        return isSuccess(ret);
    }

    public int openReadFiles(String filename) {
        return openFiles(filename);
    }

    /**
     * 打开文件
     *
     * @param fileName 被写入的文件名
     * @return
     */
    private int openFiles(String fileName) {
        byte[] fileSizeArr = new byte[0x4];
        byte[] filedata = fileName.getBytes();
        int ret = sendCommand(39, filedata.length, filedata);
        if (isSuccess(ret)) {
            System.arraycopy(filedata, 0, fileSizeArr, 0, fileSizeArr.length);
            return bytesToInt_hight2low(fileSizeArr, 0);
        }
        return ret;
    }

//    /**
//     * 获取视频文件时长
//     *
//     * @return
//     */
//    public int getVideoDurationTime(String fileName) {
//        int ret = 0;
//        if (fileName != null) {
//            byte[] bytes = strToByteArray(fileName);
//            if (bytes.length > 0 && bytes != null) {
//                ret = sendCommand(43, bytes.length, bytes);
//            }
//        }
//        return ret;
//    }

    /**
     * 获取视频文件时长
     *
     * @param fileName
     * @return
     */
    public int getVideoDurationTime(String fileName) {
        byte[] fileSizeArr = new byte[0x4];
        Log.e("downfile 打开文件 ", "downfile 打开文件 " + fileName);
        byte[] filedata = fileName.getBytes();
        int ret = sendCommand(43, filedata.length, filedata);
        Log.e("发送成功还是失败", "cmd_fd:" + UvcCamera.getInstance().cmd_fd_error + "  " + UvcCamera.getInstance().fd_error);
        if (ret >= 0) {
            System.arraycopy(filedata, 0, fileSizeArr, 0, fileSizeArr.length);
            Log.e("downfile 文件大小byte[] ", "downfile 文件大小byte[] " + bytesToHexString(fileSizeArr));
            return bytesToInt_hight2low(fileSizeArr, 0);
        }
        Log.e("downfile 获取文件的大小 ", "downfile 获取文件的大小 " + ret + UvcCamera.getInstance().cmd_fd_error);
        return ret;
    }

    /**
     * 发送帧数
     *
     * @param num
     * @return
     */
    public int getVideoZhenTime(int num) {
        byte[] fileSizeArr = new byte[0x4];
        byte[] filedata = intToBytes_hight2low(num);
        Log.e("发送帧数 ", "发送帧数 " + num + "  " + bytesToHexString(filedata));
        int ret = sendCommand(44, fileSizeArr.length, filedata);
        Log.e("发送成功还是失败2", "cmd_fd:" + UvcCamera.getInstance().cmd_fd_error + "  " + UvcCamera.getInstance().fd_error);
        if (ret >= 0) {
            System.arraycopy(filedata, 0, fileSizeArr, 0, fileSizeArr.length);
            Log.e("发送帧数文件大小byte[] ", "发送帧数 文件大小byte[] " + bytesToHexString(fileSizeArr));
            return bytesToInt_hight2low(fileSizeArr, 0);
        }
        Log.e("发送帧数 获取文件的大小 ", "发送帧数 获取文件的大小 " + ret + UvcCamera.getInstance().cmd_fd_error);
        return ret;
    }


    public static byte[] strToByteArray(String str) {
        if (str == null) {
            return null;
        }
        byte[] byteArray = str.getBytes();
        return byteArray;
    }


//    /**
//     * 获取视频时长指令 参数不一样
//     *
//     * @param request
//     * @param fileName
//     * @return
//     */
//    private int sendCommand(int request, String fileName) {
//        Log.d("zoulequan", "cmd request " + request);
//        if (UvcCamera.getInstance().isInit()) {
//            return UvcCamera.getInstance().sendCmd(Cmd_Const.NOT_USE, request, Cmd_Const.NOT_USE, Cmd_Const.NOT_USE, fileName);
//        }
//        return -1;
//    }


}

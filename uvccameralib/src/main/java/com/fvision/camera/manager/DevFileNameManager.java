package com.fvision.camera.manager;

import android.graphics.Paint;

import com.fvision.camera.bean.DevFileName;

import java.util.ArrayList;

/**
 * Created by zoulequan on 2018/12/20.
 */

public class DevFileNameManager {
    private static DevFileNameManager mDevFileNameManager;
    private static ArrayList<DevFileName> devList = new ArrayList<>();
    private static DevFileName currentDev = null;

    public static DevFileNameManager getInstance() {
        if (mDevFileNameManager == null) {
            mDevFileNameManager = new DevFileNameManager();
            //"VSFILE","HWC","com.fvision.camera" 通用版本 Vst版本
            ////"VSFILE","HWC","com.fvision.camerad" 改包名版本 针对其它车机限制包名
            //"VKFILE","HWK","com.fvision.camerakyd" 凯翼德版本
            // "MQFILE","HWC","com.xyz.mangqufuzhu" 盲区辅助版本
            //"VYFILE","HWY","com.fvision.yunovo.dvr" 云智易联版本 com.fvision.yunovo.dvr
//            devList.add(new DevFileName("DFFILE", "DFC", "com.dofun.recorder.standard"));
            //"VYFILE","HWY","com.fvision.yunovo.dvr" 云智易联
            devList.add(new DevFileName("VSFILE","HWC","com.fvision.camera"));
//            devList.add(new DevFileName("VSFILE","HWC","com.fvision.camerad"));
        }
        return mDevFileNameManager;
    }

    public ArrayList<DevFileName> getDevList() {
        return devList;
    }

    public void setCurrentDev(DevFileName dev) {
        currentDev = dev;
    }

    public DevFileName getCurrentDev() {
        return currentDev;
    }
}

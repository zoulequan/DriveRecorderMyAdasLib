package com.fvision.camera.manager;

import android.util.Log;

import com.fvision.camera.iface.ICameraStateChange;
import com.serenegiant.usb.IFrameCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mayn on 2017/12/16.
 */

public class CameraStateIml {
    private static CameraStateIml _instance;
    private ICameraStateChange mICameraStateChangeInterFace = null;
    private ICameraStateChange mICameraStateChangeCustomInterFace = null;
    private ICameraStateChange mICameraStateChangeIOnlyOnce = null;
    private List<ICameraStateChange> listenerList;

    public static CameraStateIml getInstance() {
        if (_instance == null) {
            _instance = new CameraStateIml();
        }
        return _instance;
    }

    public IFrameCallback mStateFrameCallback = new IFrameCallback() {
        @Override
        public void onFrame(byte[] frame) {
            if (frame.length < 200) {
                Log.i("onFrame", "frame.length < 200");
                return;
            }
            if (frame[frame.length - 9] != 0 &&
                    frame[frame.length - 1] != 0 &&
                    frame[frame.length - 2] != 0 &&
                    frame[frame.length - 3] != 0 &&
                    frame[frame.length - 4] != 0 &&
                    frame[frame.length - 5] != 0 &&
                    frame[frame.length - 6] != 0 &&
                    frame[frame.length - 7] != 0 &&
                    frame[frame.length - 8] != 0) {
                Log.i("onFrame", "frame[frame.length - 9]");
                return;
            }

            if (frame[0] == 0 &&
                    frame[1] == 0 &&
                    frame[2] == 0 &&
                    frame[3] == 0 &&
                    frame[4] == 0 &&
                    frame[5] == 0 &&
                    frame[6] == 0 &&
                    frame[7] == 0 &&
                    frame[8] == 0) {
                Log.i("onFrame", "frame[0] == 0");
                return;
            }

            if (CmdManager.getInstance().getCurrentState().getStateFrame() == null) {
                change(frame);
            }

            if (CmdManager.getInstance().getCurrentState().isMainStateChange(frame)) {
                change(frame);
            }
//            LogUtils.d(CameraStateUtil.bytesToHexString(frame));
//            BaseApplication.cameraState.print();
        }
    };

    private void change(byte[] frame) {
        CmdManager.getInstance().getCurrentState().setStateFrame(frame);
        if (mICameraStateChangeInterFace != null) {
            mICameraStateChangeInterFace.stateChange();
        }
        if (mICameraStateChangeIOnlyOnce != null) {
            mICameraStateChangeIOnlyOnce.stateChange();
            mICameraStateChangeIOnlyOnce = null;
        }

        if (mICameraStateChangeCustomInterFace != null) {
            mICameraStateChangeCustomInterFace.stateChange();
        }
        if (listenerList != null) {
            for (ICameraStateChange listener : listenerList) {
                listener.stateChange();
            }
        }
    }

    public void setOnCameraStateListner(ICameraStateChange icameraState) {
        mICameraStateChangeInterFace = icameraState;
    }

    public void setOnCameraStateOnlyOnceListner(ICameraStateChange icameraState) {
        mICameraStateChangeIOnlyOnce = icameraState;
    }

    public void setOnCameraStateCustomListner(ICameraStateChange icameraState) {
        mICameraStateChangeCustomInterFace = icameraState;
    }

    public void delListener(ICameraStateChange icameraState) {
        if (listenerList != null) {
            listenerList.remove(icameraState);
        }
    }

    public void addListener(ICameraStateChange icameraState) {
        if (icameraState == null) {
            return;
        }
        if (listenerList == null) {
            listenerList = new ArrayList<>();
        }
        if (listenerList.size() > 0) {
            for (ICameraStateChange back : listenerList) {
                if (back.getClass().equals(icameraState.getClass())) {
                    return;
                }
            }
        }
        listenerList.add(icameraState);
    }
}

package com.fvision.camera.util;

/**
 * Created by mayn on 2017/12/12.
 */

public class Cmd_Const {

    /////////////////////////////////////////命令类型////////////////////////////////////
    public static final int CAM_SET_MODE = 0;
    public static final int CAM_SET_REC = 1;
    public static final int CAM_SET_LOCK = 2;
    public static final int CAM_SET_PLAY = 3;
    public static final int CAM_PLAY_CUR = 4;
    public static final int CAM_SET_WATER = 5;
    public static final int CAM_SET_MUTE = 6;
    public static final int CAM_SET_PICTURE = 7;
    public static final int CAM_PLAY_NEXT = 8;
    public static final int CAM_PLAY_PREV = 9;
    public static final int CAM_SET_DURATION = 10;
    public static final int CAM_SET_TIME = 11;
    public static final int CAM_PLAY_FILE = 12;
    public static final int CAM_SET_GPS = 13;
    public static final int CAM_SET_GSENSOR_LEVEL = 14;
    public static final int CAM_SET_FORMAT = 15;
    public static final int CAM_SET_MOTION_DETECT = 16;
    public static final int CAM_SET_PASSWARD = 17;
    public static final int CAM_SET_DEL_FILE = 18;
    public static final int CAM_SET_BACK_PLAY_LOCK = 19;
    public static final int CAM_SET_PLAY_FF = 20;//Fast Forward
    public static final int CAM_SET_PLAY_FB = 21;//Fast Backward

    public static final int CAM_SET_ADAS_CODE = 22;              //0x40
    public static final int CAM_GET_ADAS_CODE = 23;              //0x40
    public static final int CAM_SET_ADAS_UID = 24;             //0x10
    public static final int CAM_GET_ADAS_UID = 25;              //0x10
    public static final int CAM_GET_DVR_UID = 26;                //0x08

    public static final int CAM_SET_VENDOR = 27;                //
    public static final int CAM_GET_VENDOR = 28;                //
    public static final int CAM_SET_VIDEO_DURATION = 29;                //
    public static final int CAM_GET_VIDEO_DURATION = 30;                //
    public static final int CAM_GET_ADAS_TYPE = 31;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * 摄像头模式 正常模式，表示工作中，录像中
     */
    public static final int CAMERA_MODEL_NORMAL = 0;
    /**
     * 摄像头模式 回放模式
     */
    public static final int CAMERA_MODEL_PLAYBACK = 1;

    /**
     * 静音状态：开
     */
    public static final int MUTE_STATE_ON = 1;

    /**
     * 静音状态：关
     */
    public static final int MUTE_STATE_OFF= 0;

    /**
     * 录像状态：录像中
     */
    public static final int RECORD_STATE_ON = 1;

    /**
     * 录像状态：未录像
     */
    public static final int RECORD_STATE_OFF = 0;

    /**
     * 录像锁状态：锁了
     */
    public static final int LOCK_STATE_ON = 1;

    /**
     * 录像锁状态：没锁
     */
    public static final int LOCK_STATE_OFF = 0;

    /**
     * 回放模式状态：回放模式
     */
    public static final int MODEL_STATE_ON = 1;

    /**
     * 回放模式状态：非回放模式
     */
    public static final int MODEL_STATE_OFF = 0;

    /**
     * 文件类型:视频
     */
    public static final int FILE_TYPE_VIDEO = 0;
    /**
     * 文件类型:图片
     */
    public static final int FILE_TYPE_PICTURE = 1;
    /**
     * 文件类型：保护
     */
    public static final int FILE_TYPE_LOCK = 2;

    /**
     * 文件播放
     */
    public static final int FILE_PLAY = 1;
    /**
     * 文件暂停
     */
    public static final int FILE_PAUSE= 0;
    /**
     * 回放列表里，FILE_BYTE_SIZE个byte表示一个文件
     */
    public static final int FILE_BYTE_SIZE = 6 ;

    /**
     * 未使用
     */
    public static final int NOT_USE = -1;
    /**
     * 执行固件升级回调
     */
    /**
     * 发送固件到记录仪回调
     */
    public static final int DEV_UPGRADE_FILEPATH_IS_EMPTY = 5;
    public static final int DEV_UPGRADE_FILE_NOT_EXISTS = 1;
    public static final int DEV_UPGRADE_IO_EXCEPTION = 2;
    public static final int DEV_UPGRADE_SENDING = 3;
    public static final int DEV_UPGRADE_FILE_WRITE_ERROR = 4;
    public static final int DEV_UPGRADE_VERSION_NOT_SUPPER = 6;
    public static final int DEV_UPGRADE_NOT_DETECTED_TF = 7;
    public static final int DEV_FILE_CHECK_NOT_DETECTED_TF = 222;
    public static final int DEV_FILE_CHECK_FAIL = 233;
    public static final int DEV_FILE_CHECK_SUCCESS = 244;
    public static final int DEV_UPGRADE_START = 255;
    public static final int CMD_SEND_ERROR = -1;
    /**
     * 下载回调
     */
    public static final int DOWNLOAD_FILE_ERROR_FILE_EXIXT = 0;
    public static final int DOWNLOAD_FILE_ERROR_OPEN_FAIL = 1;
    public static final int DOWNLOAD_FILE_ERROR_DOWNLOAD_FAIL = 2;
    public static final int DOWNLOAD_FILE_ERROR_OTHER = 3;

    /**
     * 小视频错误代码
     */
    public static final int SMALL_VIDEO_DURATION_TOO_LONG = 1;
    public static final int SMALL_VIDEO_DURATION_TOO_SMALL = 2;
    public static final int SMALL_VIDEO_RECORDING = 3;
    public static final int SMALL_VIDEO_UNDETECTED_RECORDER =4;
    public static final int SMALL_VIDEO_CMD_UNINIT = 5;
    public static final int SMALL_VIDEO_DOWNLOAD_FAIL = 6;
    public static final int SMALL_VIDEO_RECORD_STOP = 7;
    public static final int SMALL_VIDEO_FILE_EXIXT = 8;
    public static final int SMALL_VIDEO_OPEN_FAIL = 9;
    public static final int SMALL_VIDEO_VERSION_IS_TOO_LOW = 10;
    public static final int SMALL_VIDEO_OTHER = 100;
}

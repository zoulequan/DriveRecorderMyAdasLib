package com.fvision.camera.util;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.text.TextUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by mayn on 2017/12/13.
 */

public class CameraStateUtil {
    public static int bytesToInt_hight2low(byte[] src, int offset) {
        int value;
        value = (int) ((src[offset] & 0xFF)
                | ((src[offset + 1] & 0xFF) << 8)
                | ((src[offset + 2] & 0xFF) << 16)
                | ((src[offset + 3] & 0xFF) << 24));
        return value;
    }

    public static short bytesToShort(byte[] src, int offset) {
        short value;
        value = (short) ((src[offset] & 0xFF)
                | ((src[offset + 1] & 0xFF) << 8)
        );
        return value;
    }

    /**
     * byte数组转为数字，低位在前，高位在后
     *
     * @param bytes
     * @return
     */
    public static int byte2int_low2hight(byte[] bytes) {
        int length = 0;
        if (bytes.length > 0) {
            length += bytes[0] & 0xff;
        }
        if (bytes.length > 1) {
            length += bytes[1] << 8 & 0xffff;
        }
        if (bytes.length > 2) {
            length += bytes[2] << 16 & 0xffffff;
        }
        if (bytes.length > 3) {
            length += bytes[3] << 24 & 0xffffffff;
        }
        return length;
    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * 时间格式转换
     *
     * @return　时长，例：00:00:01
     */
    public static String secondToTimeString(int second) {
        String duration = "";

        int hour = second / 60 / 60;
        int m = second / 60 % 60;
        int s = second % 60;

        //     duration = (hour < 10 ? "0"+hour:hour) + ":";

        duration = duration + (m < 10 ? "0" + m : m) + ":";

        duration = duration + (s < 10 ? "0" + s : s);

        return duration;
    }

    /**
     * Long 转 String
     *
     * @param time   单位秒
     * @param format 格式　如:MM-dd
     * @return 如：3.4
     */
    public static String longToString(long time, String format) {
        if (format == null) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        //前面的lSysTime是秒数，先乘1000得到毫秒数，再转为java.util.Date类型
        Date dt = new Date(time);
        String sDateTime = sdf.format(dt);  //得到精确到秒的表示：08/31/2006 21:08:00
        return sDateTime;
    }

    /**
     * 1 -> 00001 15 -> 00015 150 -> 00150
     *
     * @param num
     * @return
     */
    public static String numFormat(int num) {
        final int length = 5;
        String str = "0000" + num;
        str = str.substring(str.length() - length, str.length());
        return str;
    }

    /**
     * 判断是不是我们的USB记录仪
     *
     * @param usbPath
     * @return
     */
    public static boolean isMyUsb(String usbPath) {
        boolean isn = false;
        File file = new File(usbPath);
        File[] list = file.listFiles();
        if (list == null || list.length < 1 || !file.isDirectory()) {
            return false;
        }
        for (File ff : list) {
            if (ff.getName().equals("VSFILE")) {
                isn = true;
            }
        }
        return isn;
    }

    /**
     * 获取app版本号
     *
     * @param context
     * @return
     */
    public static String getVersionName(Context context) {
        String version = "";
        // 获取packagemanager的实例
        PackageManager packageManager = context.getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo;
        try {
            packInfo = packageManager.getPackageInfo(context.getPackageName(),
                    0);
            version = packInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return version;
    }

    public static String getSDCardCachePath() {
        String path;
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {//
            path = Environment.getExternalStorageDirectory().getPath() + "/";
        } else {
            path = Environment.getDataDirectory().getPath() + "/";
        }
        return path;
    }


    /**
     * 判断某个Activity 界面是否在前台
     *
     * @param context
     * @param className 某个界面名称
     * @return
     */
    public static boolean isForeground(Context context, String className) {
        if (context == null || TextUtils.isEmpty(className)) {
            return false;
        }

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {
            ComponentName cpn = list.get(0).topActivity;
            if (cpn.getClassName().contains(className)) {
                return true;
            }
        }
        return false;

    }
    public static int getRandom(){
        int min=10;
        int max=100;
        Random random = new Random();
        int num = random.nextInt(max)%(max-min+1) + min;
        return num;
    }

    /**
     * 判断网络情况
     *
     * @param context 上下文
     * @return false 表示没有网络 true 表示有网络
     */
    public static boolean isNetworkAvalible(Context context) {
        // 获得网络状态管理器
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager == null) {
            return false;
        } else {
            // 建立网络数组
            NetworkInfo[] net_info = connectivityManager.getAllNetworkInfo();

            if (net_info != null) {
                for (int i = 0; i < net_info.length; i++) {
                    // 判断获得的网络状态是否是处于连接状态
                    if (net_info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

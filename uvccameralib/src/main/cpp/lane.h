#ifndef LANE_H
#define LANE_H



#define MJ_WIDTH				1280 //1280 //848
#define MJ_HEIGHT				720 //720 //480

#define LANE_GAP_W				68/2 //50
#define LANE_GAP_H				52


typedef struct tLanePositionData{
	u32 startXR , startXL , startXC ;
	u32 startYR , startYL , startYC ;
	u32 wR , wL , wC ;
	u32 hR , hL , hC ;
	float slopeR , slopeL ;
	u8 thresholdL , thresholdR , thresholdC , thresholdU ,thresholdV ;
}LANE_POSITION_DATA , *PLANE_POSITION_DATA ;


typedef struct tLaneData{
	u8 yR , uR ,vR ;
	u8 yL , uL , vL ;
}LANE_DATA , *PLANE_DATA ;

typedef struct tLdwsCtrl{
	u8 haveSensitiveData ;
	u32 sensitiveDataCnt ;
	int startTime ; // unit : s
	int curTime ;  // unit : s
	int duration ; // unit : s
	u32 threshold ; // unit : s	
}LDWS_CTRL , *PLDWS_CTRL ;


#if 1


extern int initlanePosData(void) ;
extern int setlanePosData(u8 isRight,u32 startX,u32 startY,float slope,u32 w,u32 h,u8 threshold,u8 thresholdU,u8 thresholdV) ;
extern int getlanePosData(u8 isRight,u32 * startX,u32 * startY,float * slope,u32 * w,u32 * h,u8 * threshold,u8 * thresholdU,u8 * thresholdV) ;
extern int Handler456(u8 * dataAddr , u8* isReport) ;

extern int getlanePos(u32 * startX,u32 * startY) ;
extern int setlanePos(u32 startX,u32 startY) ;

#endif

#endif


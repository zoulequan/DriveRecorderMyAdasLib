//
// Created by admin on 2017/11/7.
//

#include "mjpeg_decoder.h"

mjpeg_decoder::mjpeg_decoder() {
    mhandle=tjInitDecompress();
}
int mjpeg_decoder::mjpeg2yuv2(const unsigned char *jpegBuf, unsigned long jpegSize,
                              unsigned char *dstBuf, int width, int height) {
    return tjDecompressToYUV2(mhandle, jpegBuf, jpegSize, dstBuf, width, 1, height, TJFLAG_BOTTOMUP);
}

		
#if 0
int mjpeg_decoder::mjpeg2rgbx(const unsigned char *jpegBuf, unsigned long jpegSize,
                              unsigned char *dstBuf, int width, int height) {
    return tjDecompress2(mhandle, jpegBuf, jpegSize, dstBuf, width, 0, height, TJPF_RGB, 0);
}
#endif

mjpeg_decoder::~mjpeg_decoder() {
    tjDestroy(mhandle);
}
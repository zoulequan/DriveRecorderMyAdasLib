#ifndef UVCCAMERA_LIB_H
#define UVCCAMERA_LIB_H

#include <stdio.h>
#include <errno.h>
#include <jni.h>





#ifndef  TRUE
#define  TRUE 1
#endif
#ifndef  FALSE
#define  FALSE 0
#endif



#define ADJ_CACHE_CNT_EN 		1
#define UPLOAD_ADAS_DATA_EN 	1

#define UPLOAD_MJPEG_DATA_EN 	0 // 0-->Yuv  1-->mjpeg

#define SUPPORT_DECODE_SWITCH_EN	1

#define ANTHOER_MJ2YUV_EN 		0

#define DECODE_YUV420P_EN 		0


#define LANE_DETECT_EN			1



#define PREVIEW_PIXEL_BYTES 		3/2
#define PER_CNT_READ_SIZE 			(64*1024)


#if 1 //lgz 0207 test
	#define MAX_FRAME 		2
	#define FRAME_POOL_SZ 		MAX_FRAME + 10

	#if ADJ_CACHE_CNT_EN
		#define CACHE_FRAME_CNT		4
	#else
		#define CACHE_FRAME_CNT		0
	#endif
	
	u8 cacheFrameCnt = 0 ;

#else
	#define MAX_FRAME 		2
	#define FRAME_POOL_SZ 		MAX_FRAME + 20
#endif



typedef struct _decode_data_{
	u8 frameSize ;
	u8 readFps ;
	u8 dispFps ;
	u8 readTime ;
	u8 decodeTime ;
	
} DECODE_DATA, *PDECODE_DATA;

#if LANE_DETECT_EN
typedef struct _lane_para_{
	u32 startX ;
	u32 startY ;
	u32 w ;
	u32 h ;
	u32 threshold_y ;
	u32 threshold_u ;
	u32 threshold_v ;
	
} LANE_PARA, *PLANE_PARA;
#endif


#if 0
typedef struct {
/* --- !!! The following  data structure is forbidden to change !!! --- */
	u16 state;
	u16 total_time;
	u16 cur_time;
	u8 duration_gsensor;	// duration(rec time ) | gsensor_level (4bit)
	u8 save_1;		// no use
	u16 file_index;	// play file num
	u32 videoFrameSize ;
	u32 videoFrameNo ;
	u32 audioFrameSize ;
	u32 audioFrameNo ;
	u8 passwd[10];	
	u8 save[4];		// sav
	u32 mjpegCheckData ;
	u16 num ;
/* --- !!! The above data structure is forbidden to change !!! --- */
	u8 isReport ;
	//DECODE_DATA decodeInfo ;
} _vsd_state, *P_vsd_state;
#else

typedef  struct 
{
/* --- !!! The following  data structure is forbidden to change !!! --- */
	u16 state;
	u16 total_time;
	u16 cur_time;
	u8 duration_gsensor;	// duration(rec time ) | gsensor_level (4bit)
	u8 save_1;		// no use
	u16 file_index;	// play file num
	u32 videoFrameSize ;
	u32 videoFrameNo ;
	u32 audioFrameSize ;
	u32 audioFrameNo ;
	u8 passwd[10];	
	u8   playSpeed[4] ;		// sav
	u32 mjpegCheckData ;
	u16 num ;
/* --- !!! The above data structure is forbidden to change !!! --- */
	u8 isReport;	
	u8 channel;
	u8 App_lib;
	u32 verCode ;    // verCode �Ĵ�С��ʾ�̼��汾�¾�
	u8 devName[64] ; // DevName ��ʾ������� ���磺��gm.single.general.h65.normal.dbg��

}_vsd_state , *P_vsd_state;


#endif


typedef  struct 
{
	u16 cmd;
	u16 para_len;
	u32 paraAddr ;
}_vsd_cmd , *p_vsd_cmd;


typedef  struct 
{
	u8 cmd;
	u8 para_len;
	u8 para[256];
}_vsd_cmd_b , *p_vsd_cmd_b ;



enum anw_camera_state {
    CAM_MODE_STATE = 0x8000,
    CAM_REC_STATE = 0x4000,
    CAM_LOCK_STATE = 0x2000,
    CAM_PLAY_STATE = 0x1000,
    CAM_WATER_STATE = 0x0800,
    CAM_MUTE_STATE = 0x0400,
    CAM_SD_STATE = 0x0200,
    CAM_PIC_STATE = 0x0100,
    CAM_MOTION_DETECT = 0x0080,
    CAM_FORMAT_STATE = 0x0040,
    CAM_PLIST_STATE = 0x0020,        // 0 is not ok , 1: is ok
};





#endif


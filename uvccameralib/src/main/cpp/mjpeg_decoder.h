//
// Created by admin on 2017/11/7.
//

#ifndef UVCCAMERA_H264_MJPEG_MJPEG_DECODER_H
#define UVCCAMERA_H264_MJPEG_MJPEG_DECODER_H

extern "C"{
#include "./turbojpeg.h"
};

class mjpeg_decoder {
public:
    mjpeg_decoder();
    int mjpeg2yuv2(const unsigned char *jpegBuf, unsigned long jpegSize, unsigned char *dstBuf,
                   int width , int height);

    int mjpeg2rgbx(const unsigned char *jpegBuf, unsigned long jpegSize,
                   unsigned char *dstBuf, int width, int height);
    ~mjpeg_decoder();
private:
    tjhandle mhandle;
};


#endif //UVCCAMERA_H264_MJPEG_MJPEG_DECODER_H

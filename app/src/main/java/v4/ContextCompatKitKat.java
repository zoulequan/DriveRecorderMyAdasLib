package v4;

import android.annotation.TargetApi;
import android.content.Context;

import java.io.File;

@TargetApi(19)
class ContextCompatKitKat {
    public static File[] getExternalCacheDirs(Context context) {
        return context.getExternalCacheDirs();
    }

    public static File[] getExternalFilesDirs(Context context, String type) {
        return context.getExternalFilesDirs(type);
    }

    public static File[] getObbDirs(Context context) {
        return context.getObbDirs();
    }
}


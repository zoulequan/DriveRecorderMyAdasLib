package com.fvision.camera.base;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

import com.fvision.camera.listener.LifeCycleListener;
import com.fvision.camera.manager.ActivityStackManager;
import com.fvision.camera.utils.ToastUtils;
import com.fvision.camera.view.customview.LoadingDialog;
import com.huiying.cameramjpeg.UvcCamera;

/**
 * 基类Activity
 * 备注:所有的Activity都继承自此Activity
 * 1.规范团队开发
 * 2.统一处理Activity所需配置,初始化
 *
 * @author
 */
public abstract class BaseActivity extends Activity {

    protected Context mContext;
    private LoadingDialog progressDialog;
    protected Handler mHandler = new Handler(){};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLoadingDialog();
        if (mListener != null) {
            mListener.onCreate(savedInstanceState);
        }
        ActivityStackManager.getManager().push(this);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(getContentViewId());
        mContext = this;
        initView();
        initData();
        initListener();
        init();
    }

    protected void resetPreview(View rootview){
        Point point=new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        if(point.x<point.y){
            FrameLayout.LayoutParams layoutparams = (FrameLayout.LayoutParams) rootview.getLayoutParams();
            layoutparams.width=point.x;
            layoutparams.height=point.x*11/16;
            layoutparams.gravity= Gravity.CENTER_VERTICAL;
            Log.e("Mainactivity","width="+layoutparams.width+",height="+layoutparams.height);
            rootview.setLayoutParams(layoutparams);
        }
    }

    private void initLoadingDialog() {
        progressDialog = new LoadingDialog(this);
        progressDialog.setCancelable(false);
    }

    public void showLoadingDialog(){
        progressDialog.setMessage("请稍后...");
        progressDialog.show();
    }

    public void timeOutCloseLoadingDialog(long time){
        mHandler.postDelayed(timeOutCloseLoadThread,time);
    }

    private Runnable timeOutCloseLoadThread = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dismisLoadingDialog();
                    ToastUtils.showLongToast(getApplicationContext(),""+ UvcCamera.getInstance().cmd_fd_error);
                   // Toast.makeText(getApplicationContext(), R.string.time_out,Toast.LENGTH_SHORT).show();
                }
            });

        }
    };

    public void dismisLoadingDialog(){
        mHandler.removeCallbacks(timeOutCloseLoadThread);
        progressDialog.dismiss();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mListener != null) {
            mListener.onStart();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mListener != null) {
            mListener.onRestart();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mListener != null) {
            mListener.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mListener != null) {
            mListener.onPause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mListener != null) {
            mListener.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mListener != null) {
            mListener.onDestroy();
        }
        ActivityStackManager.getManager().remove(this);
    }

    /**
     * 获取显示view的xml文件ID
     */
    protected abstract int getContentViewId();

    /**
     * 初始化应用程序，设置一些初始化数据,获取数据等操作
     */
    protected abstract void init();

    /**
     * 初始化view
     */
    protected abstract void initView();

    protected abstract void initListener();

    /**
     * 获取上一个界面传送过来的数据
     */
    protected abstract void initData();


    /**
     * 回调函数
     */
    public LifeCycleListener mListener;

    public void setOnLifeCycleListener(LifeCycleListener listener) {
        mListener = listener;
    }


}

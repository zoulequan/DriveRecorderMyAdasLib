package com.fvision.camera.appupgrade;

import java.io.BufferedOutputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class FileOper {
	/**
     * 将String数据存为文件
     */
    public static boolean stringInertFile(String name,File file) {
    	boolean suress = false;
     byte[] b=name.getBytes();
        BufferedOutputStream stream = null;
        try {
            FileOutputStream fstream = new FileOutputStream(file,true);
            stream = new BufferedOutputStream(fstream);
            stream.write(b);
            suress=true;
        } catch (Exception e) {
            e.printStackTrace();
            suress=false;
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return suress;
    }
    
    
	/**
	 * 
	 * @param url 下载地址
	 * @param folder 下载到哪个目录
	 * @param filename 下载后的文件名
	 * @param mFileDownloadInterface
	 */
	public static void download(final String url,final String folder,final String filename,final FileDownloadInterface mFileDownloadInterface){		
		Runnable mdownRomRunnable = new Runnable() {

			@Override
			public void run() {
				try {
					URL mUrl = new URL(url);
					HttpURLConnection conn = (HttpURLConnection) mUrl
							.openConnection();
					conn.setRequestProperty("Accept-Encoding", "identity");
					conn.connect();
					int length = conn.getContentLength();
					InputStream is = conn.getInputStream();
					
					String romCacheDir = folder;
					File rom = new File(romCacheDir);
					//创建下载目录
					if (!rom.exists()) {
						rom.mkdirs();
					}
					String filepath = romCacheDir + "/"+filename;
					File ApkFile = new File(filepath);
					
					FileOutputStream fos = new FileOutputStream(ApkFile);
					
					boolean canceled = false;
					byte buf[] = new byte[1024];
					float process = 0;
					int lastProcess = 0;
					do {
						int numread = is.read(buf);
						
						if (numread <= 0) {
							//handler.sendEmptyMessage(2);
							canceled = true;
							break;
						}
						process += numread;
						float currentProcess = process /length;
						if((int)(currentProcess * 100) != lastProcess){
							mFileDownloadInterface.progress(currentProcess * 100);
						}
						lastProcess = (int)(currentProcess * 100);
						//L.e(" process "+process +"   "+(process /length) + " length = "+length);
						fos.write(buf, 0, numread);
					} while (!canceled);

					fos.close();
					is.close();
					mFileDownloadInterface.complete(filepath);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					mFileDownloadInterface.fail(0, "");
					e.printStackTrace();
				}
			}
		};
		
		Thread downLoadThread = new Thread(mdownRomRunnable);
		downLoadThread.start();
	}
	
	
}

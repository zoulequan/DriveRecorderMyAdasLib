package com.fvision.camera.appupgrade;

public interface FileDownloadInterface {
	void progress(float progress);
	void complete(String filepath);
	void fail(int code, String error);
	void update(int version_code,String path,String app_desc,String version);
}

package com.fvision.camera.iface;

/**
 * Created by mayn on 2018/1/5.
 */

public interface FroegroundIface {
    void showPopuWindow();
    void hidePopuWindow();
    void syncTime();
    void pullUsb();
}

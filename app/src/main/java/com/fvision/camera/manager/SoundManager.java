package com.fvision.camera.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;

import com.fvision.camera.utils.LogUtils;

/**
 * Created by zoulequan on 2018/7/7.
 */

public class SoundManager {
    private int model = MEDIA_PLAYER;
    public static final int MEDIA_PLAYER = 0;
    public static final int SOUND_POOL = 1;
//    public static final int OTHER = 2;
    private static SoundManager instance = null;

    public AudioManager mAudioManager;
    private Activity mActivity;

    private boolean isPlaying = true;//播报前是否有背景音乐
    private boolean adasIsPlaying = false;//ADAS是否在播报
    private boolean edogIsPlaying = false;//Edog是否在播报
    private boolean mAudioFocus = false;

    public int focus = -1;

    public static final int FOCUS_EDOG = 1;
    public static final int FOCUS_ADAS = 2;

    private static final int WHAT_ADAS_PLAY = 10;
    private static final int WHAT_EDOG_PLAY = 20;
    private static final int WHAT_MEDIA_PLAY = 30;
    public static final int WHAT_TIMEOUT_AUDIO_FOCUS = 40;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case WHAT_ADAS_PLAY:
                  //  LogUtils.write("播放ADAS声音");
                    AdasSoundManager.getInstance().playSound(msg.arg1);
                    break;
                case WHAT_EDOG_PLAY:
 //                   EdogSoundManager.getInstance().playSound(msg.arg1);
                    break;
                case WHAT_MEDIA_PLAY:
                    sendMediaButton(KeyEvent.KEYCODE_MEDIA_PLAY);
            //        LogUtils.write("继续背景音乐");
                    break;
                case WHAT_TIMEOUT_AUDIO_FOCUS:
                    abandonAudioFocus();
                    break;
            }
        }
    };

    public static SoundManager getInstance() {
        if (instance == null) {
            instance = new SoundManager();
        }
        return instance;
    }

    public void init(Activity activity) {
        mActivity = activity;
        mAudioManager = (AudioManager) mActivity.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
    }

    public boolean isAdasIsPlaying() {
        return adasIsPlaying;
    }

    public void setAdasIsPlaying(boolean adasIsPlaying) {
        this.adasIsPlaying = adasIsPlaying;
    }

    public boolean isEdogIsPlaying() {
        return edogIsPlaying;
    }

    public void setEdogIsPlaying(boolean edogIsPlaying) {
        this.edogIsPlaying = edogIsPlaying;
    }

    public void setSoundModel(int model) {
        this.model = model;
    }

    public int getSoundModel() {
        return model;
    }

    public void playBackMusic() {
//        requestAudioFocus();
//        if (mediaPlayer != null) {
//            mediaPlayer.start();
//
//        } else {
//            initBeepSound();
//            mediaPlayer.start();
//        }
        mHandler.sendEmptyMessageDelayed(WHAT_MEDIA_PLAY, 2000);
    }

    public void stopBackMusic() {

//        mActivity.startService(new Intent(mActivity,MusicService.class));

    }

    public void sendMediaButton(int keyCode) {
        boolean backMusicIsPlay = mAudioManager.isMusicActive();

        if (keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE) {
            if (!isAdasIsPlaying() && !isEdogIsPlaying()) {
                isPlaying = backMusicIsPlay;
            }

            if (!backMusicIsPlay) {
             //   LogUtils.write("背景音乐没有播放，不需要暂停");
                return;
            }
        }

        if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY) {
            if (!isPlaying) {//播报前背景音乐没开
        //        LogUtils.write("播报前背景音乐没开");
                return;
            } else {
                if (isAdasIsPlaying() || isEdogIsPlaying()) {//电子狗 ADAS 如果在播报
       //             LogUtils.write("电子狗 ADAS 在播报");
                    return;
                }
            }
        }


  //      LogUtils.write("发广播成功 " + keyCode);
        KeyEvent keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        intent.putExtra(Intent.EXTRA_KEY_EVENT, keyEvent);
        mActivity.sendOrderedBroadcast(intent, null);

    }

    public void adasPlay(int rawid) {
        adasIsPlaying = true;
//        if (getSoundModel() == SoundManager.OTHER) {
//            sendMediaButton(KeyEvent.KEYCODE_MEDIA_PAUSE);
//            LogUtils.write("暂停背景音乐");
//            Message msg = new Message();
//            msg.what = WHAT_ADAS_PLAY;
//            msg.arg1 = rawid;
//            mHandler.sendMessageDelayed(msg, 2000);
//        }else {
//            AdasSoundManager.getInstance().playSound(rawid);
//        }
        AdasSoundManager.getInstance().playSound(rawid);
    }

    public void edogPlay(int rawid) {
        edogIsPlaying = true;
//        if(model == OTHER){
//            sendMediaButton(KeyEvent.KEYCODE_MEDIA_PAUSE);
//            Message edogMsg = new Message();
//            edogMsg.what = WHAT_EDOG_PLAY;
//            edogMsg.arg1 = rawid;
//            mHandler.sendMessageDelayed(edogMsg, 1000);
//        }else {
//            EdogSoundManager.getInstance().playSound(rawid);
//        }
 //       EdogSoundManager.getInstance().playSound(rawid);
    }

    AudioManager.OnAudioFocusChangeListener afChangeListener2 = new AudioManager.OnAudioFocusChangeListener() {

        public void onAudioFocusChange(int focusChange) {
            switch (focusChange) {

            }

        }

    };


    public void requestAudioFocus() {
        LogUtils.write("正在获得音频焦点");
        if (!mAudioFocus) {
            int result = SoundManager.getInstance().mAudioManager.requestAudioFocus(afChangeListener2,
                    AudioManager.STREAM_MUSIC, // Use the music stream.
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                LogUtils.write("获得音频焦点");
                mAudioFocus = true;
                SoundManager.getInstance().focus = SoundManager.FOCUS_ADAS;
            }

        }

    }

    public void abandonAudioFocus() {
        LogUtils.write("准备失去音频焦点");
        if (mAudioFocus) {

            mAudioManager.abandonAudioFocus(afChangeListener2);

            mAudioFocus = false;
            LogUtils.write("失去音频焦点");
        }

    }

    public void timeOutAbandonAudioFocus(long duration){
        mHandler.sendEmptyMessageDelayed(WHAT_TIMEOUT_AUDIO_FOCUS,duration);
    }

    public void clearTimeOutAbandonAudioFocus(){
        mHandler.removeMessages(WHAT_TIMEOUT_AUDIO_FOCUS);
    }
}

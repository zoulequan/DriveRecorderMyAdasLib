package com.fvision.camera.view.customview;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fvision.camera.R;

/**
 * Created by admin on 2017/4/7.
 */

public class DialogAbout extends Dialog implements View.OnClickListener {


    private View.OnClickListener mUpdateClick = null;
    private TextView appversion;
    private TextView deviceversion;
    private TextView checkupdate;
    private TextView back;

    public void setUpdateClick(View.OnClickListener updateClick) {
        mUpdateClick = updateClick;
    }

    public DialogAbout(Context context) {
        this(context, R.style.dialog);
    }

    public DialogAbout(Context context, int themeResId) {
        super(context, themeResId);
        int width = getWindow().getWindowManager().getDefaultDisplay().getWidth();
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(context.getResources().getDimensionPixelOffset(R.dimen.dimen_about_width), ViewGroup.LayoutParams.WRAP_CONTENT);
        View content = LayoutInflater.from(context).inflate(R.layout.dialog_about, null);
        setContentView(content,params);
        this.back = (TextView) findViewById(R.id.back);
        this.checkupdate = (TextView) findViewById(R.id.check_update);
        this.deviceversion = (TextView) findViewById(R.id.deviceversion);
        this.appversion = (TextView) findViewById(R.id.appversion);
        getWindow().setGravity(Gravity.CENTER);
        setCanceledOnTouchOutside(false);
        setCancelable(true);
        init();
    }

    private void init() {
        back.setOnClickListener(this);
        checkupdate.setOnClickListener(this);
    }

    public DialogAbout setAppVersion(String appVersion) {
        appversion.setText(appVersion);
        return this;
    }

    public DialogAbout setDeviceVersion(String deviceVersion) {
        deviceversion.setText(deviceVersion);
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                dismiss();
                break;
            case R.id.check_update:
                if (mUpdateClick != null) {
                    mUpdateClick.onClick(v);
                }
                break;
        }
    }
}

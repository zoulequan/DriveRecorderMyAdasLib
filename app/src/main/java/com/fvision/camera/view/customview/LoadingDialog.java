package com.fvision.camera.view.customview;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;

import com.fvision.camera.R;

/**
 * Created by admin on 2017/2/21.
 */

public class LoadingDialog extends Dialog {
    private TextView tv_text;

    public LoadingDialog(Context context) {
        this(context,R.style.dialog);
    }
    public LoadingDialog(Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    private void init(){
//        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_loading);
        getWindow().setGravity(Gravity.CENTER);
        tv_text = (TextView) findViewById(R.id.tv_message);
        setCanceledOnTouchOutside(false);
        setCancelable(true);
    }

    public LoadingDialog setMessage(String message){
        tv_text.setText(message);
        return this;
    }
}

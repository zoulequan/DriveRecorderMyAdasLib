package com.fvision.camera.view.customview;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.fvision.camera.R;

/**
 * Created by admin on 2017/4/7.
 */

public class DialogSetting extends Dialog implements View.OnClickListener,CompoundButton.OnCheckedChangeListener {

    public static final String KEY_SCREENSAVER = "screensaver";
    public static final String KEY_SNAPSHOTVOICE = "snapshotvoice";
    private View.OnClickListener mFormatClick = null;
    private CheckBox cbsnapshowvoice;
    private CheckBox cbscreensaver;
    private TextView formatsd;


    public void setFormatClick(View.OnClickListener formatClick) {
        mFormatClick = formatClick;
    }

    public DialogSetting(Context context) {
        this(context, R.style.dialog);
    }

    public DialogSetting(Context context, int themeResId) {
        super(context, themeResId);
        int width = getWindow().getWindowManager().getDefaultDisplay().getWidth();
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(context.getResources().getDimensionPixelOffset(R.dimen.dimen_about_width), ViewGroup.LayoutParams.WRAP_CONTENT);
        View content = LayoutInflater.from(context).inflate(R.layout.dialog_setting, null);
        setContentView(content, params);
        this.formatsd = (TextView) findViewById(R.id.format_sd);
        this.cbscreensaver = (CheckBox) findViewById(R.id.cb_screensaver);
        this.cbsnapshowvoice = (CheckBox) findViewById(R.id.cb_snapshow_voice);

        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        init();
    }

    private void init() {
        formatsd.setOnClickListener(this);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        cbsnapshowvoice.setChecked(sp.getBoolean(KEY_SNAPSHOTVOICE,true));
        cbscreensaver.setChecked(sp.getBoolean(KEY_SCREENSAVER,true));
        cbscreensaver.setOnCheckedChangeListener(this);
        cbsnapshowvoice.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.format_sd:
                if (mFormatClick != null) {
                    mFormatClick.onClick(v);
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        switch (buttonView.getId()) {
            case R.id.cb_screensaver:
                sp.edit().putBoolean(KEY_SCREENSAVER, isChecked).commit();
                break;
            case R.id.cb_snapshow_voice:
                sp.edit().putBoolean(KEY_SNAPSHOTVOICE, isChecked).commit();
                break;
        }
    }
}

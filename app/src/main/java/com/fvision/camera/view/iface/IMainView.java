package com.fvision.camera.view.iface;

import com.fvision.camera.base.IBaseView;

/**
 * 登录view
 *
 * @author ZhongDaFeng
 */

public interface IMainView extends IBaseView {

    //状态改变刷新界面
    void resrefhView();

    void usbStateChange(String usbPath,int state);
}

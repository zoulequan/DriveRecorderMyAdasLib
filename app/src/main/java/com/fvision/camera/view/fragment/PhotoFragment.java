//package com.fvision.camera.view.fragment;//v4注释整个类
//
//
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//
//import com.fvision.camera.R;
//import com.fvision.camera.bean.FileBean;
//import com.fvision.camera.utils.CmdUtil;
//import com.fvision.camera.utils.LogUtils;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//
//import static android.content.Context.MODE_PRIVATE;
//
///**
// * A fragment with a Google +1 button.
// * Use the {@link PhotoFragment#newInstance} factory method to
// * create an instance of this fragment.
// */
//public class PhotoFragment extends Fragment {
//    // TODO: Rename parameter arguments, choose names that match
//    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "filebean";
//    // The request code must be 0 or greater.
//    // The URL to +1.  Must be a valid URL.
//    // TODO: Rename and change types of parameters
//    private String mFileBean;
//    private ImageView mImageView;
//    private CmdUtil mCmdUtil;
//    private Bitmap bitmap;
//    private int length;
//
//
//    public PhotoFragment() {
//        // Required empty public constructor
//    }
//
//    /**
//     * Use this factory method to create a new instance of
//     * this fragment using the provided parameters.
//     *
//     * @param fileBean Parameter 1.
//     * @return A new instance of fragment PhotoFragment.
//     */
//    // TODO: Rename and change types and number of parameters
//    public static PhotoFragment newInstance(String fileBean) {
//        PhotoFragment fragment = new PhotoFragment();
//        Bundle args = new Bundle();
//        args.putSerializable(ARG_PARAM1, fileBean);
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mFileBean = getArguments().getString(ARG_PARAM1);
//        }
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_photo, container, false);
//        mImageView = (ImageView) view.findViewById(R.id.imageview);
//        initImage();
//        return view;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//    }
//
////    public void saveImage() {
////        if (bitmap == null) return;
////        try {
////            FileOutputStream fos = null;
////            String path = BaseApplication.JPG_PATH + mFileBean.fileName;
////            fos = new FileOutputStream(path);
////            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
////            fos.close();
////            Toast.makeText(getActivity(), "文件已保存到" + path, Toast.LENGTH_LONG).show();
////        } catch (FileNotFoundException e) {
////            e.printStackTrace();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////    }
//
////    private void init() {
////        if (mCmdUtil.changeMode(2)) {
////            if (mCmdUtil.openReadFile(mFileBean.fileType, mFileBean.fileName)) {
////                length = mCmdUtil.getReadFileLenght();
////                int offset = 0;
////                int readlength = 0;
////                int paketLen = 0x0800;
////                byte[] photodata = new byte[length];
////                DataPaket paket = mCmdUtil.getFileData(offset, paketLen);
////                while (paket != null && paket.length > 0) {
////                    System.arraycopy(paket.data, 0, photodata, readlength, paket.length);
////                    readlength += paket.length;
////                    offset++;
////                    int len = paketLen < (length - readlength) ? paketLen : length - readlength;
////                    if (readlength < length) {
////                        paket = mCmdUtil.getFileData(offset, len);
////                    } else {
////                        paket = null;
////                    }
////                }
//////                mCmdUtil.closeReadFile();
////                if (readlength > 0) {
////                    bitmap = BitmapFactory.decodeByteArray(photodata, 0, length);
////                    mImageView.setImageBitmap(bitmap);
//////                    save.setVisibility(View.VISIBLE);
////                } else {
////                    photodata = null;
//////                    save.setVisibility(View.GONE);
////                }
//////                mCmdUtil.closeReadFile();
////            }
////        }
////    }
//
//    private void initImage() {
////        byte[] data = getImageData(mCmdUtil, mFileBean);
////        if (data != null) {
////            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
////            mImageView.setImageBitmap(bitmap);
////        }
////        Bitmap bitmap = BitmapFactory.decodeFile(mFileBean);
////        mImageView.setImageBitmap(bitmap);
////        mImageView.setImageResource(R.drawable.about);
//
////        String fileName = mFileBean.substring(mFileBean.lastIndexOf("/") + 1, mFileBean.length());
////        File img = new File(getContext().getFilesDir()+"/"+fileName);
////        FileInputStream input = null;
////        if(img == null){
////            LogUtils.e("img == null");
////            return;
////        }else {
////            LogUtils.d(img.toString()+" "+img.getPath());
////        }
////        try {
////            input= getContext().openFileInput(fileName);
////        } catch (FileNotFoundException e) {
////            e.printStackTrace();
////        }
////        Bitmap bitmap1 = BitmapFactory.decodeStream(input);
////        mImageView.setImageBitmap(bitmap1);
//        mImageView.setImageURI(Uri.fromFile(new File(mFileBean)));//这里显示
//    }
//
////    public static synchronized byte[] getImageData(CmdUtil cmdUtil, FileBean fileBean) {
////        int length = 0;
//////        if (cmdUtil.stopRecord()) {
////        if (cmdUtil.openReadFile(fileBean.fileType, fileBean.fileName)) {
////            length = cmdUtil.getReadFileLenght();
////            int offset = 0;
////            int readlength = 0;
////            int paketLen = 0x0800;
////            byte[] photodata = new byte[length];
////            DataPaket paket = cmdUtil.getFileData(offset, paketLen);
////            while (paket != null && paket.length > 0) {
////                System.arraycopy(paket.data, 0, photodata, readlength, paket.length);
////                readlength += paket.length;
////                offset++;
////                int len = paketLen < (length - readlength) ? paketLen : length - readlength;
////                if (readlength < length) {
////                    paket = cmdUtil.getFileData(offset, len);
////                } else {
////                    paket = null;
////                }
////            }
////            cmdUtil.closeReadFile();
////            if (readlength > 0) {
////                return photodata;
////            } else {
////                return null;
////            }
////        }
////        cmdUtil.closeReadFile();
//////        }
////        return null;
////    }
//}

//package com.fvision.camera.view.activity;//v4注释整个类
//
//import android.content.Intent;
//import android.graphics.Point;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import com.fvision.camera.R;
//import com.fvision.camera.App;
//
//import com.fvision.camera.view.customview.DialogHelp;
//import com.fvision.camera.view.fragment.PhotoFragment;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 图片浏览器
// */
//public class ImagePreviewActivity extends FragmentActivity implements View.OnClickListener {
//
//    public static final String KEY_FILEBEANS = "KEYFILEBEANS";
//    public static final String KEY_POSITION = "POSITION";
//    public static String ap_name="CAR-WIFI";
//    public static String ap_pwd="12345678";
//    private ImageView back;
//    private List<String> mFileBeans;
//    private ViewPager viewpager;
//    private View left, right;
//    private TextView title;
//    private View delete, connect_phone;
//    private MyPagerAdapter mPagerAdapter;
//    private int postion;
//    private View rootView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_image_preview);
//
//        this.viewpager = (ViewPager) findViewById(R.id.viewpager);
//        this.left = findViewById(R.id.iv_left);
//        title = (TextView) findViewById(R.id.title);
//        delete = findViewById(R.id.delete);
//        connect_phone = findViewById(R.id.connect_phone);
//        this.left.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (viewpager.getCurrentItem() > 0) {
//                    viewpager.setCurrentItem(viewpager.getCurrentItem() - 1);
//                }
//            }
//        });
//        this.right = findViewById(R.id.iv_right);
//        this.right.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (viewpager.getCurrentItem() < (mFileBeans.size() - 1)) {
//                    viewpager.setCurrentItem(viewpager.getCurrentItem() + 1);
//                }
//            }
//        });
//        this.back = (ImageView) findViewById(R.id.back);
//        this.back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                goBack();
//            }
//        });
//        this.rootView = findViewById(R.id.rootview);
////        this.save.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                int item=viewpager.getCurrentItem();
////                Fragment fragment = mPagerAdapter.getItem(item);
////                if(fragment instanceof PhotoFragment){
////                    ((PhotoFragment) fragment).saveImage();
////                }
////                PhotoFragment current = mPagerAdapter.getCurrentFragment();
////                if(current!=null){
////                    current.saveImage();
////                }
////            }
////        });
//        init();
//    }
//
//    private void init() {
//        mFileBeans = getIntent().getStringArrayListExtra(KEY_FILEBEANS);
//        postion = getIntent().getIntExtra(KEY_POSITION, 0);
//        if (mFileBeans != null && mFileBeans.size() > 0) {
//            mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFileBeans);
//        }
//        viewpager.setOffscreenPageLimit(0);
//        viewpager.setAdapter(mPagerAdapter);
//        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                if (position == 0) {
//                    left.setVisibility(View.GONE);
//                } else {
//                    left.setVisibility(View.VISIBLE);
//                }
//                if (position == mFileBeans.size() - 1) {
//                    right.setVisibility(View.GONE);
//                } else {
//                    right.setVisibility(View.VISIBLE);
//                }
//                setTitle();
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//        viewpager.setCurrentItem(postion);
//        delete.setOnClickListener(this);
//        connect_phone.setOnClickListener(this);
//        connect_phone.setOnClickListener(this);
//        if (postion == 0) {
//            left.setVisibility(View.GONE);
//        }
//        setTitle();
//        resetPreview(rootView);
//    }
//
//    protected void resetPreview(View rootview){
//        Point point=new Point();
//        getWindowManager().getDefaultDisplay().getSize(point);
//        if(point.x<point.y){
//            RelativeLayout.LayoutParams layoutparams = (RelativeLayout.LayoutParams) rootview.getLayoutParams();
//            layoutparams.width=point.x;
//            layoutparams.height=point.x*11/16;
//            Log.e("Mainactivity","width="+layoutparams.width+",height="+layoutparams.height);
//            rootview.setLayoutParams(layoutparams);
//        }
//    }
//
//    private void setTitle() {
//        int item = viewpager.getCurrentItem();
//        try {
//            String path = mFileBeans.get(item);
//            String name = path.substring(path.lastIndexOf("/") + 1, path.length());
//            title.setText(name);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.delete:
//                deleteImage();
//                break;
//            case R.id.connect_phone:
//                showConnectPhone();
//                break;
//        }
//    }
//
//    private DialogHelp mDialogHelp;
//    private void showConnectPhone() {
//        if (mDialogHelp == null) {
//            mDialogHelp = new DialogHelp(ImagePreviewActivity.this).setWifiName(getString(R.string.wifiname, ap_name))
//            .setWifiPwd(getString(R.string.wifipwd, ap_pwd)).setSendClick(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            try {
//                                String currentpath = mFileBeans.get(viewpager.getCurrentItem());
//                                //App.current_cutvideo = currentpath;
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//        }
//        mDialogHelp.show();
//    }
//
//    private void deleteImage() {
//        int index = viewpager.getCurrentItem();
//        String path = mFileBeans.remove(index);
//        File f = new File(path);
//        f.delete();
//        if(mFileBeans.size()>0) {
//            mPagerAdapter.notifyDataSetChanged();
//            setTitle();
//        }else{
//            goBack();
//        }
//    }
//
//    class MyPagerAdapter extends FragmentStatePagerAdapter {
//
//        private List<String> mFileBeanList;
//
//        public MyPagerAdapter(FragmentManager fm, List<String> mfiles) {
//            super(fm);
//            this.mFileBeanList = mfiles;
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return PhotoFragment.newInstance(mFileBeans.get(position));
//        }
//
//        @Override
//        public int getCount() {
//            return mFileBeanList == null ? 0 : mFileBeanList.size();
//        }
//
//        @Override
//        public int getItemPosition(Object object) {
//            return POSITION_NONE;
//        }
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            goBack();
//            return false;
//        }else {
//            return super.onKeyDown(keyCode, event);
//        }
//    }
//
//    private void goBack() {
//        Intent data = new Intent();
//        data.putStringArrayListExtra(KEY_FILEBEANS, (ArrayList<String>) mFileBeans);
//        setResult(RESULT_OK,data);
//        finish();
//    }
//}

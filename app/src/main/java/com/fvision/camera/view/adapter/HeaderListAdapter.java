package com.fvision.camera.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fvision.camera.R;
import com.fvision.camera.bean.FileBean;
import com.fvision.camera.utils.CameraStateUtil;
import com.fvision.camera.view.fragment.VideoListFragment;

import java.text.SimpleDateFormat;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;


/**
 * Created by admin on 2017/7/26.
 */

public class HeaderListAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private final List<FileBean> mValues;
    private final VideoListFragment.OnListFragmentInteractionListener mListener;
    private Context mContext;
    public HeaderListAdapter(Context context, List<FileBean> items, VideoListFragment.OnListFragmentInteractionListener listener){
        mValues = items;
        mListener = listener;
        this.mContext=context;
    }
    @Override
    public int getCount() {
        return mValues==null?0:mValues.size();
    }

    @Override
    public FileBean getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mValues.get(position).sorttime;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            holder=new ViewHolder();
            convertView= LayoutInflater.from(mContext).inflate(R.layout.fragment_videoitem,null);
            holder.filename= (TextView) convertView.findViewById(R.id.filename);
            holder.delete = (TextView) convertView.findViewById(R.id.delete);
            holder.file_time = (TextView) convertView.findViewById(R.id.file_time);
//            holder.filesize= (TextView) convertView.findViewById(R.id.filesize);
//            holder.filelength= (TextView) convertView.findViewById(R.id.filelength);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        FileBean item = mValues.get(position);
        holder.file_time.setText(CameraStateUtil.longToString(item.dayTime,null));
        holder.filename.setText(item.fileName);
//        holder.delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mListener != null) {
//                    mListener.onDelete(getItem(position));
//                }
//            }
//        });
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mListener != null) {
//                    mListener.onListFragmentInteraction(getItem(position));
//                }
//            }
//        });
//        if(item.fileType!=3&&item.fileinfo!=null) {
//            holder.filelength.setVisibility(View.VISIBLE);
//            holder.filelength.setText(getTimeStr(item.fileinfo.filelength));
//        }else{
//            holder.filelength.setVisibility(View.GONE);
//        }
//        if(item.fileinfo!=null) {
//            holder.filesize.setText(getSizeStr(item.fileinfo.filesize));
//        }else {
//            holder.filesize.setText("");
//        }
        return convertView;
    }

    private String getTimeStr(int length){
        return String.format("时长%02d分%02d秒",length/60,length%60);
    }

    private String getSizeStr(int size){
        return String.format("%.2fM",((double)size)/(1024*1024));
    }

    @Override
    public View getHeaderView(final int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.contact_header, parent, false);
            holder.text = (TextView) convertView;
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        SimpleDateFormat format=new SimpleDateFormat(mContext.getResources().getString(R.string.day_format));
 //       holder.text.setText(format.format(new Date(getItem(position).dayTime)));
        holder.text.setText(CameraStateUtil.longToString(getItem(position).dayTime,mContext.getResources().getString(R.string.day_format)));
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (null != mListener) {
//                    // Notify the active callbacks interface (the activity, if the
//                    // fragment is attached to one) that an item has been selected.
//                    mListener.onListFragmentInteraction(getItem(position));
//                }
//            }
//        });
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return getItem(position).hour;
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder{
        TextView filename;
        TextView filesize;
        TextView filelength;
        TextView delete;
        TextView file_time;
    }
}

package com.fvision.camera.view.customview;

/**
 * Created by zoulequan on 2019/2/22.
 */

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.view.MotionEvent;

import com.fvision.camera.utils.LogUtils;


public class MoveImageView extends ImageView {

    private int lastX = 0;
    private int lastY = 0;

    private static final int screenWidth = 720; //屏幕宽度
    private static final int screenHeight = 1280; //屏幕高度

    public MoveImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastX = (int) event.getRawX();
                lastY = (int) event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                int dx = (int) event.getRawX() - lastX;
                int dy = (int) event.getRawY() - lastY;

                int left = getLeft() + dx;
                int top = getTop() + dy;
                int right = getRight() + dx;
                int bottom = getBottom() + dy;
                if (left < 0) {
                    left = 0;
                    right = left + getWidth();
                }
                if (right > screenWidth) {
                    right = screenWidth;
                    left = right - getWidth();
                }
                if (top < 0) {
                    top = 0;
                    bottom = top + getHeight();
                }
                if (bottom > screenHeight) {
                    bottom = screenHeight;
                    top = bottom - getHeight();
                }
                LogUtils.d("adas draw left "+left+" top "+top + "right "+right + " bottom "+bottom);
                layout(left, top, right, bottom);
                lastX = (int) event.getRawX();
                lastY = (int) event.getRawY();
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                break;
        }
        return true;
    }

    public void layout(int x,int y){

    }
}

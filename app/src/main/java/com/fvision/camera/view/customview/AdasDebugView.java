package com.fvision.camera.view.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.fvision.camera.R;
import com.fvision.camera.manager.CmdManager;
import com.fvision.camera.utils.CameraStateUtil;
import com.fvision.camera.utils.LogUtils;
import com.huiying.cameramjpeg.UvcCamera;

public class AdasDebugView extends FrameLayout implements SeekBar.OnSeekBarChangeListener {
    private LinearLayout main, layout;
    private Button btn_hide_toggle;
    private ScrollView data_layout;
    private RadioGroup lane_select;
    private RadioButton lane_left, lane_right;
    private SeekBar start_x, start_y, slope, w, h, yu_y, yu_u, yu_v;
    private TextView text_start_x, text_start_y, text_slope, text_w, text_h, text_yu_y, text_yu_u, text_yu_v;
    private int lane = 0;//0 左 1 右
    private int value_x, value_y, value_slope, value_width, value_height, value_yu_y, value_yu_u, value_yu_v;
    private int value_x_left, value_y_left, value_slope_left, value_width_left, value_height_left, value_yu_y_left, value_yu_u_left, value_yu_v_left;

    public AdasDebugView(Context context) {
        super(context);
        init();
        // TODO Auto-generated constructor stub
    }

    public AdasDebugView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        init();
    }

    public AdasDebugView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // TODO Auto-generated constructor stub
        init();
    }

    private void init() {
        initView();
        initViewValue();
    }

    private void initView() {
        main = (LinearLayout) LayoutInflater.from(getContext()).inflate(
                R.layout.adas_debug, this, false);

        addView(main);

        layout = (LinearLayout) main.findViewById(R.id.adas_debug);
        btn_hide_toggle = (Button) main.findViewById(R.id.btn_hide_toggle);

        btn_hide_toggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                initParams();
                initViewValue();
                data_layout.setVisibility(data_layout.getVisibility() == View.VISIBLE?View.GONE:View.VISIBLE);
            }
        });

        data_layout = (ScrollView) main.findViewById(R.id.data_layout);

        start_x = (SeekBar) main.findViewById(R.id.start_x_seekbar);
        start_y = (SeekBar) main.findViewById(R.id.start_y_seekbar);
        slope = (SeekBar) main.findViewById(R.id.slope_seekbar);
        w = (SeekBar) main.findViewById(R.id.width_seekbar);
        h = (SeekBar) main.findViewById(R.id.height_seekbar);
        yu_y = (SeekBar) main.findViewById(R.id.yu_y_seekbar);
        yu_u = (SeekBar) main.findViewById(R.id.yu_u_seekbar);
        yu_v = (SeekBar) main.findViewById(R.id.yu_v_seekbar);

        start_x.setOnSeekBarChangeListener(this);
        start_y.setOnSeekBarChangeListener(this);
        slope.setOnSeekBarChangeListener(this);
        w.setOnSeekBarChangeListener(this);
        h.setOnSeekBarChangeListener(this);
        yu_y.setOnSeekBarChangeListener(this);
        yu_u.setOnSeekBarChangeListener(this);
        yu_v.setOnSeekBarChangeListener(this);

        lane_select = (RadioGroup) main.findViewById(R.id.lane_select);
        lane_left = (RadioButton) main.findViewById(R.id.lane_left);
        lane_right = (RadioButton) main.findViewById(R.id.lane_right);
        lane_select.check(lane == 0?R.id.lane_left:R.id.lane_right);
        lane_select.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == lane_left.getId()) {
                    lane = 0;
                } else if (i == lane_right.getId()) {
                    lane = 1;
                }
                setLaneParam();
                initViewValue();
            }
        });

        text_start_x = (TextView) main.findViewById(R.id.start_x);
        text_start_y = (TextView) main.findViewById(R.id.start_y);
        text_slope = (TextView) main.findViewById(R.id.slope);
        text_w = (TextView) main.findViewById(R.id.width);
        text_h = (TextView) main.findViewById(R.id.height);
        text_yu_y = (TextView) main.findViewById(R.id.yu_y);
        text_yu_u = (TextView) main.findViewById(R.id.yu_u);
        text_yu_v = (TextView) main.findViewById(R.id.yu_v);
    }

    private void initParams() {
        byte[] leftParams = new byte[64];
        UvcCamera.getInstance().getLanePara(0, leftParams);
        byteToValue(leftParams,0);
       // Log.d("adas"," leftParams "+CameraStateUtil.bytesToHexString(leftParams));
        byte[] rightParams = new byte[64];
        UvcCamera.getInstance().getLanePara(1, rightParams);
        byteToValue(rightParams,1);
    }

    private void initViewValue() {
        start_x.setProgress(lane == 0 ? value_x_left : value_x);
        start_y.setProgress(lane == 0 ? value_y_left : value_y);
        slope.setProgress(lane == 0 ? value_slope_left : value_slope);
        w.setProgress(lane == 0 ? value_width_left : value_width);
        h.setProgress(lane == 0 ? value_height_left : value_height);
        yu_y.setProgress(lane == 0 ? value_yu_y_left : value_yu_y);
        yu_u.setProgress(lane == 0 ? value_yu_u_left : value_yu_u);
        yu_v.setProgress(lane == 0 ? value_yu_v_left : value_yu_v);
    }

    private void byteToValue(byte[] bytes,int mlane) {
        if (mlane == 1) {
            value_x = byte2Short(bytes, 0, 4);
            value_y = byte2Short(bytes, 4, 4);
            value_width = byte2Short(bytes, 8, 4);
            value_height = byte2Short(bytes, 12, 4);
            value_yu_y = byte2Short(bytes, 16, 4);
            value_yu_u = byte2Short(bytes, 20, 4);
            value_yu_v = byte2Short(bytes, 24, 4);
        } else {
            value_x_left = byte2Short(bytes, 0, 4);
            value_y_left = byte2Short(bytes, 4, 4);
            value_width_left = byte2Short(bytes, 8, 4);
            value_height_left = byte2Short(bytes, 12, 4);
            value_yu_y_left = byte2Short(bytes, 16, 4);
            value_yu_u_left = byte2Short(bytes, 20, 4);
            value_yu_v_left = byte2Short(bytes, 24, 4);
        }

    }

    public void setBgColor(int color) {
        layout.setBackgroundColor(color);
    }

    public void setOnClick(OnClickListener click) {
        main.setOnClickListener(click);
    }


    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        switch (seekBar.getId()) {
            case R.id.start_x_seekbar:
                text_start_x.setText("X:" + progress);
                break;
            case R.id.start_y_seekbar:
                text_start_y.setText("Y:" + progress);
                break;
            case R.id.slope_seekbar:
                text_slope.setText("斜率:" + (progress / 100f));
                break;
            case R.id.width_seekbar:
                text_w.setText("宽度:" + progress);
                break;
            case R.id.height_seekbar:
                text_h.setText("高度:" + progress);
                break;
            case R.id.yu_y_seekbar:
                text_yu_y.setText("阈值 Y:" + progress);
                break;
            case R.id.yu_u_seekbar:
                text_yu_u.setText("阈值 U:" + progress);
                break;
            case R.id.yu_v_seekbar:
                text_yu_v.setText("阈值 V:" + progress);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        switch (seekBar.getId()) {
            case R.id.start_x_seekbar:
                if (lane == 0) {
                    value_x_left = seekBar.getProgress();
                } else {
                    value_x = seekBar.getProgress();
                }

                break;
            case R.id.start_y_seekbar:
                if (lane == 0) {
                    value_y_left = seekBar.getProgress();
                } else {
                    value_y = seekBar.getProgress();
                }
                break;
            case R.id.slope_seekbar:
                if (lane == 0) {
                    value_slope_left = seekBar.getProgress();
                } else {
                    value_slope = seekBar.getProgress();
                }
                break;
            case R.id.width_seekbar:
                if (lane == 0) {
                    value_width_left = seekBar.getProgress();
                } else {
                    value_width = seekBar.getProgress();
                }
                break;
            case R.id.height_seekbar:
                if (lane == 0) {
                    value_height_left = seekBar.getProgress();
                } else {
                    value_height = seekBar.getProgress();
                }
                break;
            case R.id.yu_y_seekbar:
                if (lane == 0) {
                    value_yu_y_left = seekBar.getProgress();
                } else {
                    value_yu_y = seekBar.getProgress();
                }
                break;
            case R.id.yu_u_seekbar:
                if (lane == 0) {
                    value_yu_u_left = seekBar.getProgress();
                } else {
                    value_yu_u = seekBar.getProgress();
                }
                break;
            case R.id.yu_v_seekbar:
                if (lane == 0) {
                    value_yu_v_left = seekBar.getProgress();
                } else {
                    value_yu_v = seekBar.getProgress();
                }
                break;
        }
        setLaneParam();
    }

    private void setLaneParam() {
        if (lane == 0) {
            UvcCamera.getInstance().setLanePara(lane, value_x_left, value_y_left, value_slope_left / 100f, value_width_left, value_height_left, value_yu_y_left, value_yu_u_left, value_yu_v_left);
        } else {
            UvcCamera.getInstance().setLanePara(lane, value_x, value_y, value_slope / 100f, value_width, value_height, value_yu_y, value_yu_u, value_yu_v);
        }
    }

    private short byte2Short(byte[] stateFrame, int startPos, int length) {
        byte[] newByte = new byte[length < 2 ? 2 : length];
        System.arraycopy(stateFrame, startPos, newByte, 0, length);
//        LogUtils.d(" newByteShort "+CameraStateUtil.bytesToHexString(newByte));
        return bytesToShort(newByte, 0);
    }

    private short bytesToShort(byte[] src, int offset) {
        short value;
        value = (short) ((src[offset] & 0xFF)
                | ((src[offset + 1] & 0xFF) << 8)
        );
        return value;
    }
}

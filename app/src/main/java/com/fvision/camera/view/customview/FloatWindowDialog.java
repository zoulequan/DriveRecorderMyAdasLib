package com.fvision.camera.view.customview;

import android.app.Dialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fvision.camera.R;

public class FloatWindowDialog extends Dialog {
	LayoutInflater inflater;
	private TextView content = null;
	private TextView title = null;
	private Button dialogCancel = null;
	private Button dialogOk = null;
	private Context mContext;
	private OnFloatWindonsClickLinteners mOnCancelClick = null;

	private View view;
	public FloatWindowDialog(Context context) {
		super(context, R.style.dialog);
		mContext = context;
		inflater = LayoutInflater.from(context);
		view = inflater.inflate(R.layout.float_windows_tips_dialog, null);
		this.setContentView(view);
		this.setCancelable(false);
		initView();
	}

	private void initView(){
		title =  view.findViewById(R.id.title);
		content =  view.findViewById(R.id.content);
		dialogCancel = view.findViewById(R.id.cancel);
		dialogOk = view.findViewById(R.id.ok);

		String tipsContent = String.format(mContext.getString(R.string.float_windows_tips_content),mContext.getString(R.string.app_name));
		content.setText(tipsContent);
		dialogCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
				if(mOnCancelClick!=null){
					mOnCancelClick.onCancel(view);
				}
			}
		});

		dialogOk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dismiss();
				if(mOnCancelClick!=null){
					mOnCancelClick.onOk(view);
				}
			}
		});

	}

	public void  setOnFloatWindonsClickLinteners(OnFloatWindonsClickLinteners onCancelClick){
		mOnCancelClick = onCancelClick;
	}

	public interface OnFloatWindonsClickLinteners{
		void onOk(View view);
		void onCancel(View view);
	}

	@Override
	public void show() {
		super.show();
	}

	public void setTitle(String msg){
		title.setText(msg);
	}

	public void setTitle(int strid){
		title.setText(strid);
	}

	@Override
	public void dismiss() {
		super.dismiss();
	}
}

package com.fvision.camera;

/**
 * Created by zoulequan on 2018/6/8.
 */

public class Confecting {
    /**
     * APP在线升级通道，专为客户定制的版本提供在线升级，不走自己的升级通道
     * APP_UPGRADE_CHANNEL 值不同，走不同的通道
     * 云选锋: ".yxf"
     * PX3:".px3"
     * .gst
     */
    public static final String APP_UPGRADE_CHANNEL = "";//px3 change gst change

    // .px3 配制说明 全局搜索 “px3 change”
    // 1,manifest 节点增加代码 android:sharedUserId="android.uid.system"
    // 2，uvccamera-lib.cpp  bool isPx3 = true;改为true
    // 3,打包APK后签 系统签名
    // 4,app/build.gradle def fileName = "DriveRecorder.apk" 改为  def fileName = "DriveRecorderPx3.apk"
    // 5，版本号改一下
    // 6，initCmd 路径替换一下 //  String[] reault = initUvcCmd(cmdpath.replace("/storage", "/mnt/media_rw")).split("_");
    /**
     * 是否隐藏小窗口
     */
    public static final boolean IS_HIDE_FLOATWINDOWS = false;
}

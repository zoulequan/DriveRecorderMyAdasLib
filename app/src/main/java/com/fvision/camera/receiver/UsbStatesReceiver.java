package com.fvision.camera.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;

import com.fvision.camera.MainActivity;
import com.fvision.camera.presenter.MainPresenter;
import com.fvision.camera.utils.CameraStateUtil;
import com.fvision.camera.utils.LogUtils;
import com.fvision.camera.utils.SharedPreferencesUtil;
import com.fvision.camera.view.iface.IMainView;
import com.huiying.cameramjpeg.UvcCamera;

public class UsbStatesReceiver extends BroadcastReceiver  {
	MainActivity execactivity;
	IMainView iMainView;
	MainPresenter presenter;
    public static final int USB_STATE_ON = 0x0;
    public static final int USB_STATE_OFF = 0x1;
    public IntentFilter filter = new IntentFilter();
    
	public UsbStatesReceiver(Context context, IMainView iview,MainPresenter presenter){
		execactivity = (MainActivity)context;
		iMainView = iview;
		this.presenter = presenter;
		filter.addAction(Intent.ACTION_MEDIA_CHECKING);
		filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		filter.addAction(Intent.ACTION_MEDIA_EJECT);
		filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
		filter.addDataScheme("file"); 	
	}

	public Intent registerReceiver() {
		return execactivity.registerReceiver(this, filter);
	}

	public void unregisterReceiver() {
		execactivity.unregisterReceiver(this);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		LogUtils.d("111111 UsbStatesReceiver "+intent.getAction());
//		iMainView.showToast(intent.getAction());
	    if(intent.getAction().equals(Intent.ACTION_MEDIA_CHECKING)){

	    }else if(intent.getAction().equals(Intent.ACTION_MEDIA_EJECT)){
			iMainView.usbStateChange(null,USB_STATE_OFF);
	    }else if(intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)){
            String path = intent.getData().getPath();
			if(path!=null && !path.equals("")){

			}
        }
	};

}

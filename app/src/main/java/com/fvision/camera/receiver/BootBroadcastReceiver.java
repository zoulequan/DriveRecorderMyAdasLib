package com.fvision.camera.receiver;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;

import com.fvision.camera.service.ForegroundService;
import com.fvision.camera.utils.CameraStateUtil;
import com.fvision.camera.utils.LogUtils;
import com.fvision.camera.utils.SharedPreferencesUtil;
import com.huiying.cameramjpeg.UvcCamera;

import v4.ContextCompat;

/**
 * Created by mayn on 2017/12/22.
 */

public class BootBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtils.d("111111 BootBroadcastReceiver onReceive "+intent.getAction());

        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){

        }else if(intent.getAction().equals("android.test.BOOT_COMPLETED")){

        }else if(intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)){

        }else if(intent.getAction().equals("android.hardware.usb.action.USB_DEVICE_ATTACHED")){

        }

        startService(context);
    }

    private void startService(Context context){
//        context.startActivity(new Intent(context, TransparentActivity.class).
//                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        LogUtils.d("111111111 startService()");
        if (Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(new Intent(context, ForegroundService.class));
        } else {
            context.startService(new Intent(context, ForegroundService.class));
        }

//        context.sendBroadcast(new Intent(Const.BROAD_CAST_SYNC_TIME));
    }

    private boolean isHavaPermission(Context context){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            return true;
        }
        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED;
    }

}

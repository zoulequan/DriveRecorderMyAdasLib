package com.fvision.camera.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtil {
	public final static String USB_PATH ="usb_path";
	public final static String TEMPORARY_USB_PATH = "temporary_usb_path";//临时存的路径
	//存储的sharedpreferences文件名
	private static final String FILE_NAME = "save_file_name";
	public final static String LAST_SUCCESS_OPEN_DEVICE_VERSION = "last_success_open_device_version";
	public final static String IS_SANCTION = "is_sanction";
	public final static String MAINACTIVITY_HAS_RUN = "mainactivity_has_run";
	public final static String CALIBRATION_X = "calibration_x";
	public final static String CALIBRATION_Y = "calibration_y";

	public static String getLastDevVersion(Context context) {
		return (String)SharedPreferencesUtil.getData(context,SharedPreferencesUtil.LAST_SUCCESS_OPEN_DEVICE_VERSION,"null");
	}

	public static void setLastDevVersion(Context context,String version) {
		saveData(context,LAST_SUCCESS_OPEN_DEVICE_VERSION,version);
	}

	public static boolean isSanction(Context context) {
		return (Boolean)SharedPreferencesUtil.getData(context,SharedPreferencesUtil.IS_SANCTION,false);
	}

	public static void setSanction(Context context,boolean isn) {
		saveData(context,IS_SANCTION,isn);
	}

	public static int getCalibrationX(Context context) {
		return (Integer)SharedPreferencesUtil.getData(context,SharedPreferencesUtil.CALIBRATION_X,0);
	}

	public static void setCalibrationX(Context context,int x) {
		saveData(context,CALIBRATION_X,x);
	}

	public static int getCalibrationY(Context context) {
		return (Integer)SharedPreferencesUtil.getData(context,SharedPreferencesUtil.CALIBRATION_Y,0);
	}

	public static void setCalibrationY(Context context,int y) {
		saveData(context,CALIBRATION_Y,y);
	}
	/**
	 * 保存数据到文件
	 * @param context
	 * @param key
	 * @param data
	 */
	public static void saveData(Context context, String key,Object data){
		LogUtils.e("saveDatasaveDatasaveDatasaveDatasaveDatasaveDatasaveDatasaveDatasaveDatasaveDatasaveDatasaveDatasaveData");
		String type = data.getClass().getSimpleName();
		SharedPreferences sharedPreferences = context
				.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();

		if ("Integer".equals(type)){
			editor.putInt(key, (Integer)data);
		}else if ("Boolean".equals(type)){
			editor.putBoolean(key, (Boolean)data);
		}else if ("String".equals(type)){
			editor.putString(key, (String)data);
		}else if ("Float".equals(type)){
			editor.putFloat(key, (Float)data);
		}else if ("Long".equals(type)){
			editor.putLong(key, (Long)data);
		}

		editor.commit();
	}

	/**
	 * 从文件中读取数据
	 * @param context
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static Object getData(Context context, String key, Object defValue){

		String type = defValue.getClass().getSimpleName();
		SharedPreferences sharedPreferences = context.getSharedPreferences
				(FILE_NAME, Context.MODE_PRIVATE);

		//defValue为为默认值，如果当前获取不到数据就返回它
		if ("Integer".equals(type)){
			return sharedPreferences.getInt(key, (Integer)defValue);
		}else if ("Boolean".equals(type)){
			return sharedPreferences.getBoolean(key, (Boolean)defValue);
		}else if ("String".equals(type)){
			return sharedPreferences.getString(key, (String)defValue);
		}else if ("Float".equals(type)){
			return sharedPreferences.getFloat(key, (Float)defValue);
		}else if ("Long".equals(type)){
			return sharedPreferences.getLong(key, (Long)defValue);
		}

		return null;
	}

}

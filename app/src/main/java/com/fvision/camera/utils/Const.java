package com.fvision.camera.utils;

import android.os.Environment;

/**
 * Created by mayn on 2017/12/22.
 */

public class Const {
    public static final String ROOT_PATH= Environment.getExternalStorageDirectory()+"/uvccameramjpeg/";
    public static final String JPG_PATH=ROOT_PATH+"JPEG/";

    public static final String BROAD_CAST_SHOW_FLOATWINDOW = "android.action.show.floatwindow";
    public static final String BROAD_CAST_HIDE_FLOATWINDOW = "android.action.hide.floatwindow";

    public static final String isPendingIntent = "isPendingIntent";
    public static final String BROAD_CAST_SYNC_TIME = "broadcast.sync.time";
}
